package nova.academia.academic;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.server.Responsive;
import com.vaadin.server.VaadinSession;
import com.vaadin.ui.Button;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.Component;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.Grid;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Notification;
import com.vaadin.ui.PasswordField;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Grid.SingleSelectionModel;
import com.vaadin.ui.Notification.Type;
import com.vaadin.ui.themes.ValoTheme;

import nova.academia.util.Controller;
import nova.academia.util.State;
import nova.academia.util.Storage;

public class SessionView extends HorizontalLayout{

	private static boolean isEdit;
	private Controller c;
	private Session user;
	private BeanItemContainer<Session> container;
	

	private TextField session;
	private TextField begin;
	private TextField end;
	private ComboBox status;
	private Grid grid;
	
	public static boolean isEdit() {
		return isEdit;
	}

	public static void setEdit(boolean isEdit) {
		SessionView.isEdit = isEdit;
	}

	
	public SessionView(){
		
		Responsive.makeResponsive(this);
		
		setWidth("100%");
		setSizeFull();
		setMargin(true);
		setSpacing(true);
		//setComponentAlignment(form, Alignment.MIDDLE_CENTER);
		addStyleName("users-view");
		addStyleName("material");
			
		addComponent(buildUser());
		addComponent(buildTable());
		
		try {
			onLoad();
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}
	
	private FormLayout buildUser(){
		
		FormLayout form = new FormLayout();
		form.setSizeFull();
		form.setCaption("Create Session");
		
		session = new TextField("Session");
		session.setWidth("80%");
		form.addComponent(session);
		
		begin = new TextField("Session Begins");
		begin.setWidth("80%");
		form.addComponent(begin);
		
		end = new TextField("Session Ends");
		end.setWidth("80%");
		form.addComponent(end);
		
		status = new ComboBox("Status");
		status.setWidth("80%");
		status.addItems("OPEN", "CLOSED");
		form.addComponent(status);
				
		HorizontalLayout hb = new HorizontalLayout();
		hb.setSpacing(true);
		hb.setMargin(true);
		hb.setWidth("80%");
		
		Button save = new Button("Open Session");
		save.addClickListener(event -> {
			try {
				fireSaveEvent();
			} catch (JSONException e) {
				e.printStackTrace();
			}
		});
		save.addStyleName(ValoTheme.BUTTON_FRIENDLY);
		
				
		hb.addComponent(save);
		
		form.addComponent(hb);
		
				
		return form;
		
		
	}
	
	private Component buildTable(){
	
		container = new BeanItemContainer<Session>(Session.class);
		
		// Create a grid
		grid = new Grid("Session");
	
		grid.addColumn("session");
		grid.addColumn("begin");
		grid.addColumn("end");
		grid.addColumn("status");
		
				
		Grid.Column b = grid.getColumn("begin");
		b.setHeaderCaption("Session Begins");
		
		Grid.Column c = grid.getColumn("end");
		c.setHeaderCaption("Session End");
		
		grid.setSizeFull();
		grid.setResponsive(true);
		grid.setEditorEnabled(true);
		
		grid.addSelectionListener(selectionEvent -> { 
			
			Session selected = (Session)((SingleSelectionModel)grid.getSelectionModel()).getSelectedRow();
			if (selected != null){
				session.setValue(selected.getSession());
				begin.setValue(selected.getBegin());
				end.setValue(selected.getEnd());
				status.setValue(selected.getStatus());
				Storage.setUserid(selected.getSessionID());
				setEdit(true);
			}
			else{
				Notification.show("Nothing selected");
			}
		});
		
		return grid;
	}
	
	private void fireSaveEvent() throws JSONException {
		c = new Controller();
		
		JSONObject json = new JSONObject();
		json.put("tenantID", VaadinSession.getCurrent().getAttribute("tenantID"));
		json.put("session", session.getValue());
		json.put("begins", begin.getValue());
		json.put("ends", end.getValue());
		json.put("status", status.getValue());

		
		if(!isEdit){
			System.out.println("Saving Object");
			String h = c.postRequest("admin/session", json);
			JSONObject oj = new JSONObject(h);
			JSONObject j = oj.getJSONObject("response");
			if(j != null){
				Notification.show("Session Created", Type.HUMANIZED_MESSAGE);
				clear();
				onLoad();
			}
		}else{
			json.put("sessionID", Storage.getUserid());
			String h = c.postRequest("admin/sessionu", json);
			JSONObject j = new JSONObject(h);
			if(j.getInt("response") == 1){
				Notification.show("Session Data Updated", Type.HUMANIZED_MESSAGE);
				onLoad();
				clear();
//				setEdit(false);
			}else{
				Notification.show("Session Data Failed to Update", Type.HUMANIZED_MESSAGE);
				clear();
				setEdit(false);
			}
		}
	}
	
	private void onLoad() throws JSONException{
		c = new Controller();
		container.removeAllItems();
		
		JSONObject o = new JSONObject();
		o.put("tenantID", VaadinSession.getCurrent().getAttribute("tenantID"));
		String response = c.postRequest("admin/sessions", o);
		
		if(response != null){
			
			JSONObject jp = new JSONObject(response);
			JSONArray aj = jp.getJSONArray("response");
			
			for(int i = 0; i < aj.length(); i++){
				user = new Session();
				JSONObject j = aj.getJSONObject(i);
				user.setSessionID(j.getString("sessionID"));
				user.setSession(j.getString("session"));
				user.setBegin(j.getString("begins"));
				user.setEnd(j.getString("ends"));
				user.setStatus(j.getString("status"));
				
				container.addBean(user);
			}
						
			grid.setContainerDataSource(container);
		}
		
	}
	
	private void clear(){
		setEdit(false);
		session.setValue("");
		begin.setValue("");
		end.setValue("");
	}
	
}

