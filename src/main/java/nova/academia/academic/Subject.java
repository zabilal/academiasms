package nova.academia.academic;

public class Subject {

	private long id;
	private String subjectName;
	private String examTotal;
	private  String assessTotal;
	private String subID;

	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getSubjectName() {
		return subjectName;
	}
	public void setSubjectName(String subjectName) {
		this.subjectName = subjectName;
	}
	
	public String getExamTotal() {
		return examTotal;
	}
	public void setExamTotal(String examTotal) {
		this.examTotal = examTotal;
	}
	public String getAssessTotal() {
		return assessTotal;
	}
	public void setAssessTotal(String assessTotal) {
		this.assessTotal = assessTotal;
	}
	public String getSubID() {
		return subID;
	}
	public void setSubID(String subID) {
		this.subID = subID;
	}
	@Override
	public String toString() {
		return "Subject [id=" + id + ", subjectName=" + subjectName + ", examTotal=" + examTotal + ", assessTotal="
				+ assessTotal + ", subID=" + subID + "]";
	}
	
}
