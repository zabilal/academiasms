package nova.academia.academic;

public class Session {

	private String sessionID;
	private String session;
	private String begin;
	private String end;
	private String tenantID;
	private String status;
	
	public String getSessionID() {
		return sessionID;
	}
	public void setSessionID(String sessionID) {
		this.sessionID = sessionID;
	}
	public String getSession() {
		return session;
	}
	public void setSession(String session) {
		this.session = session;
	}
	public String getBegin() {
		return begin;
	}
	public void setBegin(String begin) {
		this.begin = begin;
	}
	public String getEnd() {
		return end;
	}
	public void setEnd(String end) {
		this.end = end;
	}
	public String getTenantID() {
		return tenantID;
	}
	public void setTenantID(String tenantID) {
		this.tenantID = tenantID;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	@Override
	public String toString() {
		return "Session [sessionID=" + sessionID + ", session=" + session + ", begin=" + begin + ", end=" + end
				+ ", tenantID=" + tenantID + ", status=" + status + "]";
	}
	
}
