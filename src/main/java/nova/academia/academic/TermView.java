package nova.academia.academic;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.server.Responsive;
import com.vaadin.server.VaadinSession;
import com.vaadin.ui.Button;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.Component;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.Grid;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Notification;
import com.vaadin.ui.PasswordField;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Grid.SingleSelectionModel;
import com.vaadin.ui.Notification.Type;
import com.vaadin.ui.themes.ValoTheme;

import nova.academia.util.Controller;
import nova.academia.util.State;
import nova.academia.util.Storage;

public class TermView extends HorizontalLayout{

	private static boolean isEdit;
	private Controller c;
	private Term user;
	private BeanItemContainer<Term> container;
	

	private TextField term;
	private TextField begin;
	private TextField end;
	private ComboBox status;
	private Grid grid;
	
	public static boolean isEdit() {
		return isEdit;
	}

	public static void setEdit(boolean isEdit) {
		TermView.isEdit = isEdit;
	}

	
	public TermView(){
		
		Responsive.makeResponsive(this);
		
		setWidth("100%");
		setSizeFull();
		setMargin(true);
		setSpacing(true);
		//setComponentAlignment(form, Alignment.MIDDLE_CENTER);
		addStyleName("users-view");
		addStyleName("material");
			
		addComponent(buildUser());
		addComponent(buildTable());
		
		try {
			onLoad();
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}
	
	private FormLayout buildUser(){
		
		FormLayout form = new FormLayout();
		form.setSizeFull();
		form.setCaption("Create Term");
		
		term = new TextField("Term");
		term.setWidth("80%");
		form.addComponent(term);
		
		begin = new TextField("Term Begins");
		begin.setWidth("80%");
		form.addComponent(begin);
		
		end = new TextField("Term Ends");
		end.setWidth("80%");
		form.addComponent(end);
		
		status = new ComboBox("Status");
		status.setWidth("80%");
		status.addItems("Open", "Closed");
		form.addComponent(status);
				
		HorizontalLayout hb = new HorizontalLayout();
		hb.setSpacing(true);
		hb.setMargin(true);
		hb.setWidth("80%");
		
		Button save = new Button("Add Term");
		save.addClickListener(event -> {
			try {
				fireSaveEvent();
			} catch (JSONException e) {
				e.printStackTrace();
			}
		});
		save.addStyleName(ValoTheme.BUTTON_FRIENDLY);
		
				
		hb.addComponent(save);
		
		form.addComponent(hb);
		
				
		return form;
		
		
	}
	
	private Component buildTable(){
	
		container = new BeanItemContainer<Term>(Term.class);
		
		// Create a grid
		grid = new Grid("Terms");
	
		grid.addColumn("term");
		grid.addColumn("begin");
		grid.addColumn("end");
		grid.addColumn("status");
		
				
		Grid.Column b = grid.getColumn("begin");
		b.setHeaderCaption("Term Begins");
		
		Grid.Column c = grid.getColumn("end");
		c.setHeaderCaption("Term End");
		
		grid.setSizeFull();
		grid.setResponsive(true);
		grid.setEditorEnabled(true);
		
		grid.addSelectionListener(selectionEvent -> { 
			
			Term selected = (Term)((SingleSelectionModel)grid.getSelectionModel()).getSelectedRow();
			if (selected != null){
				term.setValue(selected.getTerm());
				begin.setValue(selected.getBegin());
				end.setValue(selected.getEnd());
				status.setValue(selected.getStatus());
				Storage.setUserid(selected.getTermID());
				setEdit(true);
			}
			else{
				Notification.show("Nothing selected");
			}
		});
		
		return grid;
	}
	
	private void fireSaveEvent() throws JSONException {
		c = new Controller();
		
		JSONObject json = new JSONObject();
		json.put("tenantID", VaadinSession.getCurrent().getAttribute("tenantID"));
		json.put("term", term.getValue());
		json.put("begins", begin.getValue());
		json.put("ends", end.getValue());
		json.put("status", status.getValue());

		
		if(!isEdit){
			System.out.println("Saving Object");
			String h = c.postRequest("admin/term", json);
			JSONObject oj = new JSONObject(h);
			JSONObject j = oj.getJSONObject("response");
			if(j != null){
				Notification.show("Term Added", Type.HUMANIZED_MESSAGE);
				clear();
				onLoad();
			}
		}else{
			json.put("termID", Storage.getUserid());
			String h = c.postRequest("admin/termus", json);
			JSONObject j = new JSONObject(h);
			if(j.getInt("response") == 1){
				Notification.show("Term Updated", Type.HUMANIZED_MESSAGE);
				onLoad();
				clear();
//				setEdit(false);
			}else{
				Notification.show("Term Failed to Update", Type.HUMANIZED_MESSAGE);
			}
		}
	}
	
	private void onLoad() throws JSONException{
		c = new Controller();
		container.removeAllItems();
		
		JSONObject o = new JSONObject();
		o.put("tenantID", VaadinSession.getCurrent().getAttribute("tenantID"));
		String response = c.postRequest("admin/terms", o);
		
		if(response != null){
			
			JSONObject jp = new JSONObject(response);
			JSONArray aj = jp.getJSONArray("response");
			
			for(int i = 0; i < aj.length(); i++){
				user = new Term();
				JSONObject j = aj.getJSONObject(i);
				user.setTermID(j.getString("termID"));
				user.setTerm(j.getString("term"));
				user.setBegin(j.getString("begins"));
				user.setEnd(j.getString("ends"));
				user.setStatus(j.getString("status"));
				
				container.addBean(user);
			}
						
			grid.setContainerDataSource(container);
		}
		
	}
	
	private void clear(){
		setEdit(false);
		term.setValue("");
		begin.setValue("");
		end.setValue("");
		status.setValue("");
	}
	
}

