package nova.academia.academic;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.server.Responsive;
import com.vaadin.server.VaadinSession;
import com.vaadin.ui.Button;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.Component;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.Grid;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Notification;
import com.vaadin.ui.PasswordField;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Grid.SingleSelectionModel;
import com.vaadin.ui.Notification.Type;
import com.vaadin.ui.themes.ValoTheme;

import nova.academia.util.Controller;
import nova.academia.util.State;
import nova.academia.util.Storage;

public class SubjectView extends HorizontalLayout{

	private static boolean isEdit;
	private Controller c;
	private Subject user;
	private BeanItemContainer<Subject> container;
	

	private TextField subjectname;
	private TextField exam;
	private TextField assess;
	private Grid grid;
	
	public static boolean isEdit() {
		return isEdit;
	}

	public static void setEdit(boolean isEdit) {
		SubjectView.isEdit = isEdit;
	}

	
	public SubjectView(){
		
		Responsive.makeResponsive(this);
		
		setWidth("100%");
		setSizeFull();
		setMargin(true);
		setSpacing(true);
		//setComponentAlignment(form, Alignment.MIDDLE_CENTER);
		addStyleName("users-view");
		addStyleName("material");
			
		addComponent(buildUser());
		addComponent(buildTable());
		
		try {
			onLoad();
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}
	
	private FormLayout buildUser(){
		
		FormLayout form = new FormLayout();
		form.setSizeFull();
		form.setCaption("New Subject");
		
		subjectname = new TextField("Subject Name");
		subjectname.setWidth("80%");
		form.addComponent(subjectname);
		
		exam = new TextField("Total Exam Mark");
		exam.setWidth("80%");
		form.addComponent(exam);
		
		assess = new TextField("Total Assessment Mark");
		assess.setWidth("80%");
		form.addComponent(assess);
				
		HorizontalLayout hb = new HorizontalLayout();
		hb.setSpacing(true);
		hb.setMargin(true);
		hb.setWidth("80%");
		
		Button save = new Button("Add Subject");
		save.addClickListener(event -> {
			try {
				fireSaveEvent();
			} catch (JSONException e) {
				e.printStackTrace();
			}
		});
		save.addStyleName(ValoTheme.BUTTON_FRIENDLY);
		
				
		hb.addComponent(save);
		
		form.addComponent(hb);
		
				
		return form;
		
		
	}
	
	private Component buildTable(){
	
		container = new BeanItemContainer<Subject>(Subject.class);
		
		// Create a grid
		grid = new Grid("Subject List");
	
		grid.addColumn("subjectName");
		grid.addColumn("assessTotal");
		grid.addColumn("examTotal");
		
		Grid.Column a = grid.getColumn("subjectName");
		a.setHeaderCaption("Subject Name");
		
		Grid.Column b = grid.getColumn("assessTotal");
		b.setHeaderCaption("Total Assessment Mark.");
		
		Grid.Column c = grid.getColumn("examTotal");
		c.setHeaderCaption("Total Exam Mark");
		
		grid.setSizeFull();
		grid.setResponsive(true);
		
		grid.addSelectionListener(selectionEvent -> { 
			
			Subject selected = (Subject)((SingleSelectionModel)grid.getSelectionModel()).getSelectedRow();
			if (selected != null){
				subjectname.setValue(selected.getSubjectName());
				exam.setValue(String.valueOf(selected.getExamTotal()));
				assess.setValue(String.valueOf(selected.getAssessTotal()));
				Storage.setUserid(selected.getSubID());
				setEdit(true);
			}
			else{
				Notification.show("Nothing selected");
			}
		});
		
		return grid;
	}
	
	private void fireSaveEvent() throws JSONException {
		c = new Controller();
		
		JSONObject json = new JSONObject();
		json.put("tenantID", VaadinSession.getCurrent().getAttribute("tenantID"));
		json.put("subjectName", subjectname.getValue());
		json.put("assessTotal", assess.getValue());
		json.put("examTotal", exam.getValue());

		
		if(!isEdit){
			System.out.println("Saving Object");
			String h = c.postRequest("admin/subject", json);
			JSONObject oj = new JSONObject(h);
			JSONObject j = oj.getJSONObject("response");
			if(j != null){
				Notification.show("Subject Saved", Type.HUMANIZED_MESSAGE);
				clear();
				onLoad();
			}
		}else{
			json.put("subID", Storage.getUserid());
			String h = c.postRequest("admin/subjectu", json);
			JSONObject j = new JSONObject(h);
			if(j.getInt("response") == 1){
				Notification.show("Subject Updated", Type.HUMANIZED_MESSAGE);
				onLoad();
				clear();
//				setEdit(false);
			}else{
				Notification.show("Subject Failed to Update", Type.HUMANIZED_MESSAGE);
			}
		}
	}
	
	private void onLoad() throws JSONException{
		c = new Controller();
		container.removeAllItems();
		
		JSONObject o = new JSONObject();
		o.put("tenantID", VaadinSession.getCurrent().getAttribute("tenantID"));
		String response = c.postRequest("admin/subjects", o);
		
		if(response != null){
			
			JSONObject jp = new JSONObject(response);
			JSONArray aj = jp.getJSONArray("response");
			
			for(int i = 0; i < aj.length(); i++){
				user = new Subject();
				JSONObject j = aj.getJSONObject(i);
				user.setSubID(j.getString("subID"));
				user.setAssessTotal(j.getString("assessTotal"));
				user.setExamTotal(j.getString("examTotal"));
				user.setSubjectName(j.getString("subjectName"));
				
				container.addBean(user);
			}
						
			grid.setContainerDataSource(container);
		}
		
	}
	
	private void clear(){
		setEdit(false);
		subjectname.setValue("");
		exam.setValue("");
		assess.setValue("");
	}
	
}
