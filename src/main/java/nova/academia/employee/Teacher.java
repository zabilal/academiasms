package nova.academia.employee;

import java.util.ArrayList;
import java.util.Iterator;

import org.apache.commons.codec.binary.Base64;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.vaadin.server.FontAwesome;
import com.vaadin.server.Responsive;
import com.vaadin.server.ThemeResource;
import com.vaadin.server.VaadinSession;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.Component;
import com.vaadin.ui.Embedded;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Notification;
import com.vaadin.ui.OptionGroup;
import com.vaadin.ui.Panel;
import com.vaadin.ui.PasswordField;
import com.vaadin.ui.TabSheet;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Notification.Type;
import com.vaadin.ui.themes.ValoTheme;

import nova.academia.assessment.AssessView;
import nova.academia.util.Controller;
import nova.academia.util.Notify;
import nova.academia.util.State;

public class Teacher extends VerticalLayout{
	
	private static boolean isEdit;
	
	private static int count,current;
	private TabSheet tabsheet;
	private Button next;
	
	private TextField firstname;
	private TextField lastname;
	private ComboBox gender;
	private ComboBox marital;
	
	private TextField address;
	private TextField city;
	private TextField state;
	private TextField country;
	private TextField phone;
	private TextField email;
	
	private TextField date;
	private TextField salary;
	private TextField desig;
	private TextField cert;
	
	private ComboBox role;
	private TextField username;
	private PasswordField password;
	
	private OptionGroup klassmulti, submulti;

	private ArrayList<String> sub = new ArrayList<>();
	private ArrayList<String> kl = new ArrayList<>();
	private static int subsize, klsize;
	
	private Controller c;
	
	public static boolean isEdit() {
		return isEdit;
	}

	public static void setEdit(boolean isEdit) {
		Teacher.isEdit = isEdit;
	}
	
	public Teacher(){
		Responsive.makeResponsive(this);
		
//		setHeight("100%");
//		setSizeFull();
		setMargin(true);
		setSpacing(true);
		//setComponentAlignment(form, Alignment.MIDDLE_CENTER);
		addStyleName("users-view");
		addStyleName("material");
		setEdit(false);
		
		try {
			popCombo();
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
		addComponent(buildTabs());
	}
	
	private VerticalLayout buildTabs(){
		
		VerticalLayout lay = new VerticalLayout();
		lay.setMargin(true);
		lay.setSpacing(true);
		lay.setHeight("100%");
		
		tabsheet = new TabSheet();
		lay.addComponent(tabsheet);
		
		tabsheet.addTab(buildTab1(), "Personal Information");
		tabsheet.addTab(buildTab2(), "Contact Information");
		tabsheet.addTab(buildTab3(), "Other Information");
		tabsheet.addTab(buildTab4(), "Privileges");
		tabsheet.addTab(buildTab5(), "Login Details");
		
		current = tabsheet.getTabIndex();
		System.out.println("Tab Count : " + count +"\n" + "Current Tab Index : " + current);
		
		HorizontalLayout hb = new HorizontalLayout();
//		hb.setSpacing(true);
		hb.setMargin(true);
		hb.setWidth("100%");
		
		next = new Button("Next", FontAwesome.ARROW_RIGHT);
		next.addStyleName(ValoTheme.BUTTON_FRIENDLY);
		next.addStyleName(ValoTheme.BUTTON_ICON_ALIGN_RIGHT);
		next.addClickListener(e -> {

			confusion(e.getButton().getCaption());
		});
		
		Button back = new Button("Back", FontAwesome.ARROW_LEFT);
		back.addStyleName(ValoTheme.BUTTON_FRIENDLY);
		back.addClickListener(e -> {
			
			if(current >= count){
				current = count - 1;
			}
			tabsheet.setSelectedTab(current-1);
			next.setCaption("Next");
			current -= 1;
		});
		hb.addComponents(back, next);
		
		lay.addComponent(hb);
		
		return lay;
	}

	private Component buildTab1() {
		VerticalLayout tab1 = new VerticalLayout();
		tab1.setMargin(true);
		tab1.setSpacing(true);
		
		firstname = new TextField("First Name");
		firstname.setWidth("80%");
		tab1.addComponent(firstname);
		
		lastname = new TextField("Last Name");
		lastname.setWidth("80%");
		tab1.addComponent(lastname);
		
		gender = new ComboBox("Gender");
		gender.setWidth("80%");
		gender.addItems("MALE", "FEMALE");
		tab1.addComponent(gender);
		
		marital = new ComboBox("Marital Status");
		marital.setWidth("80%");
		marital.addItems("SINGLE", "MARRIED");
		tab1.addComponent(marital);
		
		return tab1;
	}
	
	private Component buildTab2() {
		VerticalLayout tab1 = new VerticalLayout();
		tab1.setMargin(true);
		tab1.setSpacing(true);
		
		address = new TextField("Address");
		address.setWidth("80%");
		tab1.addComponent(address);
		
		city = new TextField("City");
		city.setWidth("80%");
		tab1.addComponent(city);
		
		state = new TextField("State");
		state.setWidth("80%");
		tab1.addComponent(state);
		
		country = new TextField("Country");
		country.setWidth("80%");
		tab1.addComponent(country);
		
		phone = new TextField("Phone");
		phone.setWidth("80%");
		tab1.addComponent(phone);
		
		email = new TextField("Email");
		email.setWidth("80%");
		tab1.addComponent(email);
		
		return tab1;
	}
	
	private Component buildTab3() {
		VerticalLayout tab1 = new VerticalLayout();
		tab1.setMargin(true);
		tab1.setSpacing(true);
		
		date = new TextField("Date Employed");
		date.setWidth("80%");
		tab1.addComponent(date);
		
		salary = new TextField("Salary");
		salary.setWidth("80%");
		tab1.addComponent(salary);
		
		desig = new TextField("Designation","e.g Head Teacher, Class Teacher or Game Master");
		desig.setWidth("80%");
		tab1.addComponent(desig);
		
		cert = new TextField("Qualifications");
		cert.setWidth("80%");
		tab1.addComponent(cert);
		
		return tab1;
	}
	
	private Component buildTab4() {
		VerticalLayout tab1 = new VerticalLayout();
		tab1.setMargin(true);
		tab1.setSpacing(true);
		
		Label title = new Label("Teacher Privileges");
		title.setPrimaryStyleName("view-title");
		tab1.addComponent(title);
		
		HorizontalLayout grid = new HorizontalLayout();
		grid.setMargin(true);
		grid.setSpacing(true);
		
		VerticalLayout sgrid = new VerticalLayout();
		sgrid.setMargin(true);
		sgrid.setSpacing(true);
		sgrid.setWidth("100%");
		
		VerticalLayout cgrid = new VerticalLayout();
		cgrid.setMargin(true);
		cgrid.setSpacing(true);
		cgrid.setWidth("100%");
		
		submulti = new OptionGroup("Select Subject Privileges");
		submulti.setMultiSelect(true);
		
		for(int i = 0; i < subsize; i++){
			submulti.addItem(sub.get(i));
			submulti.addValueChangeListener(e->{
				Notify.notify(submulti.getValue().toString());
			});
			
		}
		sgrid.addComponent(submulti);
		
		klassmulti = new OptionGroup("Select Class Privileges");
		klassmulti.setMultiSelect(true);
		for(int i = 0; i < klsize; i++){
			klassmulti.addItem(kl.get(i));
			klassmulti.addValueChangeListener(e->{
				Notify.notify(klassmulti.getValue().toString());
			});
			
		}
		cgrid.addComponent(klassmulti);
		Panel pan1 = new Panel("Subject Privileges");
		pan1.setWidth("400px");
		Panel pan2 = new Panel("Class Privileges");
		pan2.setWidth("400px");
		
		pan1.setContent(sgrid);
		grid.addComponent(pan1);
		grid.setComponentAlignment(pan1, Alignment.MIDDLE_LEFT);
		pan2.setContent(cgrid);
		grid.addComponent(pan2);
		grid.setComponentAlignment(pan2, Alignment.MIDDLE_RIGHT);
		
		tab1.addComponent(grid);
		
		return tab1;
	}
	
	private Component buildTab5() {
		
		FormLayout form = new FormLayout();
		form.setSizeFull();
		form.setCaption("Add Users");
		
		role = new ComboBox("User Role");
		role.setWidth("80%");
		role.addItem(State.TEACHER);
		form.addComponent(role);
		
		username = new TextField("Username");
		username.setWidth("80%");
		form.addComponent(username);
		
		password = new PasswordField("Password");
		password.setWidth("80%");
		form.addComponent(password);
	
		return form;
	}
	
	private void popCombo() throws JSONException{
		c = new Controller();
		JSONObject o = new JSONObject();
		o.put("tenantID", VaadinSession.getCurrent().getAttribute("tenantID"));
		String response = c.postRequest("admin/subjects", o);
		String response2 = c.postRequest("admin/classes", o);
		
		if(response != null && response2 != null){
			
			JSONObject jp = new JSONObject(response);
			JSONArray aj = jp.getJSONArray("response");
			subsize = aj.length();
			
			JSONObject ab = new JSONObject(response2);
			JSONArray b = ab.getJSONArray("response");
			klsize = b.length();
			
			for(int i = 0; i < b.length(); i++){
				JSONObject k = b.getJSONObject(i);
				kl.add(k.getString("klassName"));
				
			}
			for(int j = 0; j < aj.length(); j++ ){
				JSONObject d = aj.getJSONObject(j);
				sub.add(d.getString("subjectName"));
			}
				
		}
	}

	private void confusion(String caption){
		//Incase of Any issue this code in outer if goes the next button event
		if(caption.equalsIgnoreCase("next")){
			count = tabsheet.getComponentCount();
			if(current < 0){
				current = 0;
			}
			if(current == (count-2)){
				next.setCaption("Submit");
			}
			tabsheet.setSelectedTab(current+1);
			current += 1;
		}else{
			try {
				next.setEnabled(false);
				fireSaveEvent();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	private void fireSaveEvent() throws JSONException{
		
		c = new Controller();
		
		JSONObject json = new JSONObject();
		JSONObject json2 = new JSONObject();
		json.put("tenantID", VaadinSession.getCurrent().getAttribute("tenantID"));
		json.put("firstname", firstname.getValue());
		json.put("lastname", lastname.getValue());
		json.put("gender", gender.getValue());
		json.put("empdate", date.getValue());
		json.put("city", city.getValue());
		json.put("state", state.getValue());
		json.put("country", country.getValue());
		json.put("phone", phone.getValue());
		json.put("email", email.getValue());
		json.put("address", address.getValue());
		json.put("salary", salary.getValue());
		json.put("desig", desig.getValue());
		json.put("cert", cert.getValue());
		json.put("marital", marital.getValue());
		json.put("subp", submulti.getValue());
		json.put("klassp", klassmulti.getValue());
		
		json2.put("tenantID", VaadinSession.getCurrent().getAttribute("tenantID"));
		json2.put("fullname", firstname.getValue() + " " + lastname.getValue());
		json2.put("username", username.getValue());
		json2.put("password", password.getValue());
		json2.put("role", role.getValue());
		json2.put("phone", phone.getValue());
		json2.put("email", email.getValue());
		
		if(!isEdit){
			String h = c.postRequest("admin/teacher", json);
			String h2 = c.postRequest("admin/inter2", json2);
			
			JSONObject oj = new JSONObject(h);
			JSONObject j = oj.getJSONObject("response");
			
			JSONObject oj2 = new JSONObject(h2);
			JSONObject j2 = oj2.getJSONObject("response");
			if(j != null && j2 != null){
				Notify.notify("Teacher Information Saved");
				clear();
				next.setEnabled(true);
			}else{
				Notify.notify("Teacher Information Failed to Save");
			}
		}else{
			String h = c.postRequest("admin/teachu", json);
			JSONObject j = new JSONObject(h);
			if(j.getInt("response") == 1){
				Notification.show("Teacher Information Updated", Type.HUMANIZED_MESSAGE);
			}else{
				Notification.show("Teacher Information Failed to Update", Type.HUMANIZED_MESSAGE);
			}
		}
	}
	
	private void clear(){
		firstname.setValue("");
		lastname.setValue("");
		gender.setValue("");
		date.setValue(null);
		state.setValue("");
		city.setValue("");
		country.setValue("");
		marital.setValue("");
		address.setValue("");
		salary.setValue("");
		desig.setValue("");
		cert.setValue("");
		phone.setValue("");
		email.setValue("");
		role.setValue("");
		username.setValue("");
		password.setValue("");
		klassmulti.clear();
		submulti.clear();
	}

}
