package nova.academia.admission;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.vaadin.addon.contextmenu.ContextMenu;
import com.vaadin.addon.contextmenu.MenuItem;
import com.vaadin.data.Item;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.data.util.GeneratedPropertyContainer;
import com.vaadin.data.util.PropertyValueGenerator;
import com.vaadin.data.util.filter.SimpleStringFilter;
import com.vaadin.server.VaadinSession;
import com.vaadin.ui.Component;
import com.vaadin.ui.Grid;
import com.vaadin.ui.Notification;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import com.vaadin.ui.renderers.ButtonRenderer;
import com.vaadin.ui.Grid.HeaderCell;
import com.vaadin.ui.Grid.HeaderRow;
import com.vaadin.ui.Grid.SingleSelectionModel;

import nova.academia.settings.entity.User;
import nova.academia.util.Controller;
import nova.academia.util.Notify;


public class Student2View extends VerticalLayout{

	
	private Grid grid;
	private BeanItemContainer<Student> container;
	private Controller c;
	private Student user;
	
	public Student2View() {
		
		setPrimaryStyleName("material");
		addStyleName("setup-view");
		setWidth("80%");
		setMargin(true);
		setSpacing(true);
		
		addComponent(buildTable());
		
		try {
			onLoad();
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}
	
	private Component buildTable(){
		container = new BeanItemContainer<Student>(Student.class);
				
		grid = new Grid("Student List");
		
		grid.addColumn("regno");
		grid.addColumn("surname");
		grid.addColumn("other");
		grid.addColumn("klass");
		grid.setSizeFull();
		grid.setResponsive(true);
		
		Grid.Column regColumn = grid.getColumn("regno");
		regColumn.setHeaderCaption("Registeration No.");
		
		Grid.Column firstColumn = grid.getColumn("surname");
		firstColumn.setHeaderCaption("First Name");
				
		Grid.Column otherColumn = grid.getColumn("other");
		otherColumn.setHeaderCaption("Last Name");
		
		Grid.Column klassColumn = grid.getColumn("klass");
		klassColumn.setHeaderCaption("Current Class");
		
		grid.addSelectionListener(selectionEvent -> { 
			
			// Create a context menu for 'someComponent'
			ContextMenu contextMenu = new ContextMenu(grid, true);
			contextMenu.setAsContextMenuOf(grid);
			
			Student selected = (Student)((SingleSelectionModel)grid.getSelectionModel()).getSelectedRow();
			if (selected != null){

				final MenuItem item = contextMenu.addItem("Edit Student", e -> {
					Window win = new Window();
					win.setContent(new Student3View(selected));
					win.setModal(true);
					win.setClosable(true);
					this.getUI().addWindow(win);
				});
				final MenuItem item2 = contextMenu.addItem("Delete Student", e -> {
		            
		            try {
						fireDelete(selected);
					} catch (JSONException e1) {
						e1.printStackTrace();
					}
		        });
			}
			else{
				Notification.show("Nothing selected");
			}
		});
		
		filterProvider(grid);
		
		return grid;
	}
	
	private void fireDelete(Student selected) throws JSONException {
		c = new Controller();
		JSONObject json = new JSONObject();
		json.put("tenantID", VaadinSession.getCurrent().getAttribute("tenantID"));
		json.put("studentID", selected.getStudentID());
		
		String h = c.postRequest("admin/studentd", json);
		JSONObject j = new JSONObject(h);
		if(j.getInt("response") == 1){
			Notify.notify("Student Record Deleted");
			onLoad();
		}else{
			Notify.notify("Student Record Failed to Delete");
		}
		
	}

	private void onLoad() throws JSONException{
		c = new Controller();
		container.removeAllItems();
		
		JSONObject o = new JSONObject();
		o.put("tenantID", VaadinSession.getCurrent().getAttribute("tenantID"));
		String response = c.postRequest("admin/allstudent", o);
		
		if(response != null){
			
			JSONObject jp = new JSONObject(response);
			JSONArray aj = jp.getJSONArray("response");
			
			for(int i = 0; i < aj.length(); i++){
				user = new Student();
				JSONObject j = aj.getJSONObject(i);
				user.setId(j.getLong("id"));
				user.setRegno(j.getString("regno"));
				user.setSurname(j.getString("surname"));
				user.setOther(j.getString("other"));
				user.setKlass(j.getString("klass"));
				user.setTenantID(j.getString("tenantID"));
				user.setAddress(j.getString("address"));
				user.setComment(j.getString("comment"));
				user.setCountry(j.getString("country"));
				user.setDates(j.getString("date"));
				user.setEmail(j.getString("email"));
				user.setFullname(j.getString("fullname"));
				user.setGender(j.getString("gender"));
				user.setHouse(j.getString("house"));
				user.setLga(j.getString("lga"));
				user.setMiddle(j.getString("middle"));
				user.setPhone(j.getString("phone"));
				user.setState(j.getString("state"));
				user.setStatus(j.getString("status"));
				user.setStudentID(j.getString("studentID"));
				
				container.addBean(user);
				
			}
						
			grid.setContainerDataSource(container);
		}
		
	}

	private void filterProvider(Grid grid){

				HeaderRow filterRow = grid.appendHeaderRow();
				for (Object pid: grid.getContainerDataSource().getContainerPropertyIds()) {
					HeaderCell cell = filterRow.getCell(pid);
					TextField filterField = new TextField();
					filterField.setHeight("25px");
					filterField.setColumns(8);
					filterField.addTextChangeListener(change -> {
					container.removeContainerFilters(pid);
						if (!change.getText().isEmpty())
							container.addContainerFilter(new SimpleStringFilter(pid, change.getText(), true, false));
					});
					cell.setComponent(filterField);
				}
	}
}
