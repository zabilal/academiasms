package nova.academia.admission;

import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import javax.imageio.ImageIO;

import org.apache.commons.codec.binary.Base64;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.vaadin.easyuploads.UploadField;
import org.vaadin.easyuploads.UploadField.FieldType;

import com.vaadin.icons.VaadinIcons;
import com.vaadin.server.Responsive;
import com.vaadin.server.StreamResource;
import com.vaadin.server.VaadinSession;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.DateField;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Image;
import com.vaadin.ui.Label;
import com.vaadin.ui.Notification;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Notification.Type;
import nova.academia.util.Controller;
import nova.academia.util.Notify;

public class StudentView extends VerticalLayout{
	
	private TextField surname;
	private TextField middle;
	private TextField other;
	private ComboBox gender;
	private DateField date;
	private TextField state;
	private TextField lga;
	private TextField country;
	private TextArea comment;
	private TextField regno;
	private ComboBox klass;
	private ComboBox house;
	private ComboBox status;
	private TextField fullname;
	private TextField phone;
	private TextField email;
	private TextField address;
	private Button save;
	private byte[] logo;
	private UploadField up;
	private Image image;
	
	private Controller c;
	private static boolean isEdit;
	
	public static boolean isEdit() {
		return isEdit;
	}

	public static void setEdit(boolean isEdit) {
		StudentView.isEdit = isEdit;
	}

	public StudentView(){
		
		Responsive.makeResponsive(this);
		
		Label title = new Label("New Student Registeration");
		title.setPrimaryStyleName("view-title");
		
		setPrimaryStyleName("student-view");
		addStyleName("material");
		addComponent(title);
		setSpacing(true);
		setMargin(true);
		setWidth("100%");

		addComponent(buildField());
		
		try {
			popCombo();
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
	}
	
	private HorizontalLayout buildField(){
		HorizontalLayout v = new HorizontalLayout();
		v.setSpacing(true);
		v.setMargin(true);
		v.setWidth("100%");
		
		v.addComponent(buildLeft());
		v.addComponent(buildRight());
		return v;
	}
	
	private VerticalLayout buildLeft(){
		VerticalLayout v = new VerticalLayout();
		v.setSpacing(true);
		v.setMargin(true);
		v.setWidth("100%");
		
		surname = new TextField("Surname");
		surname.setWidth("80%");
		v.addComponent(surname);
		middle = new TextField("Middle Name");
		middle.setWidth("80%");
		v.addComponent(middle);
		other = new TextField("Other Name");
		other.setWidth("80%");
		v.addComponent(other);
		gender = new ComboBox("Gender");
		gender.setWidth("80%");
		gender.addItems("MALE", "FEMALE");
		v.addComponent(gender);
		date = new DateField("Date of Birth");
		//date.setValue("");
		date.setDateFormat("dd-MM-yyyy");
		date.setWidth("80%");
		v.addComponent(date);
		lga = new TextField("L.G.A");
		lga.setWidth("80%");
		v.addComponent(lga);
		state = new TextField("State of Origin");
		state.setWidth("80%");
		v.addComponent(state);
		country = new TextField("Nationality");
		country.setWidth("80%");
		v.addComponent(country);
		comment = new TextArea("Any Comments(Health and Others)");
		comment.setWidth("80%");
		v.addComponent(comment);

		
		return v;
	}
	
	private VerticalLayout buildRight(){
		VerticalLayout v = new VerticalLayout();
		v.setSpacing(true);
		v.setMargin(true);
		v.setWidth("100%");
		
		regno = new TextField("Registeration Number");
		regno.setWidth("80%");
		v.addComponent(regno);
		klass = new ComboBox("Class of Admission");
		klass.setWidth("80%");
		v.addComponent(klass);
		house = new ComboBox("House");
		house.setWidth("80%");
		house.addItems("GREEN", "ORANGE");
		v.addComponent(house);
		status = new ComboBox("Admission Status");
		status.addItems("ADMITTED","PENDING", "PROBATON", "SUSPENDED", "GRADUATED");
		status.setWidth("80%");
		v.addComponent(status);
		
		Label title = new Label("Parents/Guardian Information");
		title.setWidth("80%");
		v.addComponent(title);
		
		fullname = new TextField("Full Name");
		fullname.setWidth("80%");
		v.addComponent(fullname);
		phone = new TextField("Phone Number");
		phone.setWidth("80%");
		v.addComponent(phone);
		email = new TextField("Email Address");
		email.setWidth("80%");
		v.addComponent(email);
		address = new TextField("Contact Address");
		address.setWidth("80%");
		v.addComponent(address);
		
		image = new Image();
		up= new UploadField();
		up.setButtonCaption("Select Passport...");
		up.setWidth("50%");
		up.setCaption("Student Passport");
		up.setAcceptFilter("image/*");
		up.setFieldType(FieldType.BYTE_ARRAY);
		up.addListener(new Listener(){
	        @Override
	        public void componentEvent(Event event)
	        {
	        	Object value = up.getValue();
	            logo = (byte[]) value;
	            showUploadedImage(up, image);
	        }
	    });
		v.addComponent(image);
		v.addComponent(up);
		
		HorizontalLayout hb = new HorizontalLayout();
		hb.setSpacing(true);
		hb.setMargin(true);
		hb.setWidth("90%");
		
		save = new Button(VaadinIcons.PLUS);
		save.addStyleName("floating-action");
		save.addClickListener(event -> {
			try {
//				fireSaveEvent();
				print();
				
			} catch (Exception e) {
				e.printStackTrace();
			}
		});
		hb.addComponent(save);
		hb.setComponentAlignment(save, Alignment.MIDDLE_RIGHT);
		v.addComponent(hb);
		
		
		
		return v;
	}

	private void showUploadedImage(UploadField upload, Image image) {
	    Object value = upload.getValue();
	    final byte[] data = (byte[]) value;
	 
	    StreamResource resource = new StreamResource(
	        new StreamResource.StreamSource() {
	            @Override
	            public InputStream getStream() {
	                 try {
	                  BufferedImage bi = ImageIO.read(new ByteArrayInputStream(data));
	                  
	                  int scaleX = (int) (bi.getWidth() * 0.25);
	                  int scaleY = (int) (bi.getHeight() * 0.25);
	                  
	                  java.awt.Image newImg = bi.getScaledInstance(scaleX, scaleY, java.awt.Image.SCALE_SMOOTH);
	                  /* Write the image to a buffer. */
	                  ByteArrayOutputStream imagebuffer = new ByteArrayOutputStream();
	                  BufferedImage bimage = new BufferedImage(scaleX, scaleY, BufferedImage.TYPE_INT_RGB);
	                  Graphics bg = bimage.getGraphics();
	                  bg.drawImage(newImg, 0, 0, null);
	                  bg.dispose();
	                  ImageIO.write(bimage, "jpg", imagebuffer);
	                  
	                  /* Return a stream from the buffer. */
	                  return new ByteArrayInputStream(imagebuffer.toByteArray());//data);//
	                } catch (Exception e) {
	                    return null;
	                }
	            }
	        }, "filename.png");
	    
	    image.setSource(resource);
//	    image.setVisible(true);
	}

	private void fireSaveEvent() throws JSONException {
		// TODO Auto-generated method stub
		save.setEnabled(false);
		c = new Controller();
		String logos = Base64.encodeBase64String(logo);
		
		JSONObject json = new JSONObject();
		json.put("tenantID", VaadinSession.getCurrent().getAttribute("tenantID"));
		json.put("surname", surname.getValue());
		json.put("middle", middle.getValue());
		json.put("other", other.getValue());
		json.put("gender", gender.getValue());
		json.put("date", date.getValue());
		json.put("lga", lga.getValue());
		json.put("state", state.getValue());
		json.put("country", country.getValue());
		json.put("phone", phone.getValue());
		json.put("email", email.getValue());
		json.put("comment", comment.getValue());
		json.put("regno", regno.getValue());
		json.put("klass", klass.getValue());
		json.put("house", house.getValue());
		json.put("status", status.getValue());
		json.put("fullname", fullname.getValue());
		json.put("address", address.getValue());
		//json.put("passport", logos);
		
		
		if(!isEdit){
			json.put("logo", logos);
			String h = c.postRequest("admin/student", json);
			JSONObject oj = new JSONObject(h);
			JSONObject j = oj.getJSONObject("response");
			if(j != null){
				Notify.notify("Student Registeration Saved");
				clear();
				save.setEnabled(true);
			}else{
				Notify.notify("Student Registerarion not Saved");
			}
		}else{
			String h = c.postRequest("admin/studentu", json);
			JSONObject j = new JSONObject(h);
			if(j.getInt("response") == 1){
				Notification.show("Student Information Updated", Type.HUMANIZED_MESSAGE);
			}else{
				Notification.show("School Information Failed to Update", Type.HUMANIZED_MESSAGE);
			}
		}
	}

	private void popCombo() throws JSONException{
		c = new Controller();
		JSONObject o = new JSONObject();
		o.put("tenantID", VaadinSession.getCurrent().getAttribute("tenantID"));
		String response2 = c.postRequest("admin/classes", o);
		
		if(response2 != null){
			
			JSONObject ab = new JSONObject(response2);
			JSONArray b = ab.getJSONArray("response");
			
			for(int i = 0; i < b.length(); i++){
			
				JSONObject k = b.getJSONObject(i);
				klass.addItem(k.getString("klassName"));
				
			}
				
		}
	}
	
	private void clear(){
		surname.setValue("");
		middle.setValue("");
		other.setValue("");
		gender.setValue("");
		date.setValue(null);
		state.setValue("");
		lga.setValue("");
		country.setValue("");
		comment.setValue("");
		regno.setValue("");
		klass.setValue("");
		house.setValue("");
		status.setValue("");
		fullname.setValue("");
		phone.setValue("");
		email.setValue("");
		address.setValue("");
		image.setSource(null);
	}

	private void print(){
		
//		Window win = new Window();
//		win.setContent(new Report());
//		win.setModal(true);
//		win.setClosable(true);
//		this.getUI().addWindow(win);
	}
}
