package nova.academia.util;

public class State {
	public static final String TEACHER = "TEACHER";
	public static final String ADMIN = "ADMIN";
	public static final String PARENT = "PARENT";
	public static final String STUDENT = "STUDENT";
	public static final String ACCOUNTANT = "ACCOUNTANT";
	public static final String LIBRARIAN = "LIBRARIAN";
	public static final String FRONTDESK = "FRONTDESK";
	public static final String STOREKEEPER = "STOREKEEPER";
	
}
