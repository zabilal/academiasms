package nova.academia.util;

public class Storage {
	
	private static long id;
	private String user;
	private String pass;
	private static String tenantID;
	private String role;
	private String schoolName;
	private String dept;
	private static String userid;
	private static String regno;
	
	
	
	public static long getId() {
		return id;
	}
	public static void setId(long id) {
		Storage.id = id;
	}
	public String getUser() {
		return user;
	}
	public void setUser(String user) {
		this.user = user;
	}
	public String getPass() {
		return pass;
	}
	public void setPass(String pass) {
		this.pass = pass;
	}
	public static String getTenantID() {
		return tenantID;
	}
	public static void setTenantID(String tenantID) {
		Storage.tenantID = tenantID;
	}
	public String getRole() {
		return role;
	}
	public void setRole(String role) {
		this.role = role;
	}
	public String getSchoolName() {
		return schoolName;
	}
	public void setSchoolName(String schoolName) {
		this.schoolName = schoolName;
	}
	public String getDept() {
		return dept;
	}
	public void setDept(String dept) {
		this.dept = dept;
	}
	public static String getUserid() {
		return userid;
	}
	public static void setUserid(String userid) {
		Storage.userid = userid;
	}
	public static String getRegno() {
		return regno;
	}
	public static void setRegno(String regno) {
		Storage.regno = regno;
	}
	@Override
	public String toString() {
		return "Storage [user=" + user + ", pass=" + pass + ", role=" + role + ", schoolName=" + schoolName + ", dept="
				+ dept + "]";
	}
	
}
