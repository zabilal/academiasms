package nova.academia.util;


import static net.sf.dynamicreports.report.builder.DynamicReports.*;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.imageio.ImageIO;

import org.apache.commons.codec.binary.Base64;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.vaadin.server.VaadinSession;
import java.io.ByteArrayOutputStream;

//4.1.1
//import net.sf.dynamicreports.examples.Templates;
import net.sf.dynamicreports.jasper.builder.JasperReportBuilder;
import net.sf.dynamicreports.report.builder.DynamicReports;
import net.sf.dynamicreports.report.builder.column.TextColumnBuilder;
import net.sf.dynamicreports.report.builder.datatype.BigDecimalType;
import net.sf.dynamicreports.report.builder.style.StyleBuilder;
import net.sf.dynamicreports.report.constant.HorizontalTextAlignment;
import net.sf.dynamicreports.report.constant.VerticalTextAlignment;
import net.sf.dynamicreports.report.datasource.DRDataSource;
import net.sf.dynamicreports.report.exception.DRException;
import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;
import nova.academia.admission.Student;
import nova.academia.reports.ClassPosition;
import nova.academia.reports.Score;



public class Report{
	
	private Controller c;
	private String logo, pic, names, regno, klass, term;
	private byte[] logos, photo;
	private Student user;
	private Score score;
	private List<Score> scores;
	private String subject, grade, remark;
	private int pos;
	public int rows = 0;
	
	
	public Report(Student std, String term, String session) throws DRException {

		logo = schoolLogo();
                logos = Base64.decodeBase64(logo);

                pic = getStudentInfo(std);
                photo = Base64.decodeBase64(pic) ;
        
        
                try {
                            scores = pullResults(std, term, session);
                    } catch (JSONException e) {
                            e.printStackTrace();
                    }
        
//		build(std, term, session);
		
	}
	
	//Setting Report Template Components and their LAF
		public ByteArrayOutputStream build(Student std, String term, String session) {
                    
                    ByteArrayOutputStream out = new ByteArrayOutputStream();
                    
			Report.CurrencyType currencyType = new Report.CurrencyType();

			StyleBuilder boldStyle         = stl.style().bold();
			StyleBuilder boldCenteredStyle = stl.style().setHorizontalTextAlignment(HorizontalTextAlignment.CENTER);
			StyleBuilder columnTitleStyle  = stl.style(boldCenteredStyle)
			                                    .setBorder(stl.pen1Point())
			                                    .setBackgroundColor(Color.BLUE);
//	                StyleBuilder columnStyle  = stl.style(boldCenteredStyle)
//			                                    .setBorder(stl.pen2Point());
	                
	                StyleBuilder boldTitleStyle = stl.style(boldStyle).setHorizontalTextAlignment(HorizontalTextAlignment.CENTER);
	                
	                StyleBuilder titleStyle        = stl.style(boldTitleStyle)
			                                    .setVerticalTextAlignment(VerticalTextAlignment.MIDDLE)
			                                    .setFontSize(20).setForegroundColor(Color.GREEN);
	                
	                StyleBuilder titleStyleMoto        = stl.style(boldTitleStyle)
			                                    .setVerticalTextAlignment(VerticalTextAlignment.MIDDLE)
			                                    .setFontSize(15).setForegroundColor(Color.BLUE);
	                
	                StyleBuilder titleStyle1        = stl.style(boldTitleStyle)
			                                    .setVerticalTextAlignment(VerticalTextAlignment.MIDDLE)
			                                    .setFontSize(15).setForegroundColor(Color.red);
	                StyleBuilder titleStyle2        = stl.style()
			                                    .setVerticalTextAlignment(VerticalTextAlignment.MIDDLE)
			                                    .setFontSize(10);
	                
	                StyleBuilder titleStyle3        = stl.style()
			                                    .setVerticalTextAlignment(VerticalTextAlignment.MIDDLE)
			                                    .setFontSize(10);
	                
	              
	               
	                //                                                           title,     field name     data type
	                TextColumnBuilder<String>     subjectColumn      = col.column("Subject",       "subject",      type.stringType()).setFixedWidth(100);//.setStyle(boldStyle);
	                TextColumnBuilder<Integer>    cwColumn  = col.column("C/W",   "cw",  type.integerType()).setStyle(boldCenteredStyle);
	                TextColumnBuilder<Integer>    hwColumn  = col.column("H/W",   "hw",  type.integerType()).setStyle(boldCenteredStyle);
	                TextColumnBuilder<Integer>    midColumn  = col.column("MIDTERM",   "mid",  type.integerType()).setStyle(boldCenteredStyle);
	                TextColumnBuilder<Integer>    examColumn  = col.column("EXAM",   "exam",  type.integerType()).setStyle(boldCenteredStyle);
	                TextColumnBuilder  totalColumn  = midColumn.add(examColumn).setTitle("Total").setStyle(boldCenteredStyle).setDataType(currencyType);//col.column("TOTAL",   "total",  type.integerType()).setStyle(boldCenteredStyle);
	                TextColumnBuilder<String>     gradeColumn      = col.column("GRADE",       "grade",      type.stringType()).setStyle(boldCenteredStyle);
	                TextColumnBuilder<Integer>    positionColumn  = col.column("POSITION",   "pos",  type.integerType()).setStyle(boldCenteredStyle);
	                TextColumnBuilder<String>     remarkColumn      = col.column("REMARK",       "remark",      type.stringType()).setStyle(boldCenteredStyle);
	                
	                TextColumnBuilder<String>     affectColumn      = col.column("AFFECTIVE ASSESSMENT",       "affect",      type.stringType());//.setStyle(boldStyle);
	                TextColumnBuilder<String>     ratingColumn      = col.column("RATING",       "rate",      type.stringType()).setStyle(boldCenteredStyle);
	                
	                TextColumnBuilder<String>     psychColumn      = col.column("PSYCHOMOTOR ASSESSMENT",       "psych",      type.stringType());//.setStyle(boldStyle);
	                TextColumnBuilder<String>     rateColumn      = col.column("RATING",       "rat",      type.stringType()).setStyle(boldCenteredStyle);
	                
//	                TextColumnBuilder<Integer>    rowNumberColumn = col.reportRowNumberColumn("No.");
	                //Lets start building the report template
	                try {
	                	JasperReportBuilder report = DynamicReports.report();
	                            report.setColumnTitleStyle(columnTitleStyle)
	                            .setSubtotalStyle(boldStyle)
	                            .highlightDetailEvenRows()
//	                            .setColumnStyle(columnStyle)
	                            
	                            .title(//shows Main report title along with any image to serve as a logo.
	                                    cmp.horizontalList()
	                                            .add(
	                                                    //This first line is supposed to display an image on the
	                                                    //title as a logo, but am still fighting with it.
	                                                    cmp.image(createImageFromBytes(logos)).setFixedDimension(80, 80),
	                                                    cmp.verticalList().add(cmp.text("DARUL-ARQAM ISLAMIC SCHOOLS").setStyle(titleStyle).setHorizontalTextAlignment(HorizontalTextAlignment.CENTER),
	                                                            cmp.text("Fountain of Knowledge").setStyle(titleStyleMoto).setHorizontalTextAlignment(HorizontalTextAlignment.CENTER), 
	                                                            cmp.text("Barkin Sale, Minna Niger State").setStyle(titleStyle2).setHorizontalTextAlignment(HorizontalTextAlignment.CENTER), 
	                                                            cmp.text("darul-arqam@gmail.com, 08186401028").setStyle(titleStyle2).setHorizontalTextAlignment(HorizontalTextAlignment.CENTER)),
	                                                    cmp.image(createImageFromBytes(photo)).setFixedDimension(80, 80)
	                                            )
	                                            .newRow()
	                                            .add(cmp.filler().setStyle(stl.style().setTopBorder(stl.pen2Point())).setFixedHeight(10)))
	                            
	                            .title(//shows Second report title Container for Student Details.
	                                    cmp.horizontalList()
	                                            .add(
	                                                    //This first line is supposed to display an image on the
	                                                    //title as a logo, but am still fighting with it.
	                                                    cmp.text("Name : "+""+names+""+"\n"+"Admission No: "+""+regno+""+"\n"+"Class : "+""+klass+"").setStyle(titleStyle2).setHorizontalTextAlignment(HorizontalTextAlignment.LEFT),
	                                                    cmp.text("Position in Class : "+""+getPosition(std, term, session)+""+"\n"+"Number in Class : "+""+getNumberInClass(std)+"").setStyle(titleStyle2).setHorizontalTextAlignment(HorizontalTextAlignment.LEFT)
//	                                                    cmp.text("Age : "+"\n"+"Days Present : "+"\n"+"Days Absent : " ).setStyle(titleStyle2).setHorizontalTextAlignment(HorizontalTextAlignment.LEFT)
	                                                    )
	                                            .newRow()
	                                            .add(cmp.filler().setStyle(stl.style().setTopBorder(stl.pen2Point())).setFixedHeight(10)))
	                            
	                            .title(//shows Third report title.
	                                    cmp.horizontalList()
	                                            .add(cmp.text("Report Sheet For First Term 2016/2017").setStyle(titleStyle1).setHorizontalTextAlignment(HorizontalTextAlignment.CENTER)))
	                            
	                            .columns(//add columns
	                            		 subjectColumn, cwColumn, hwColumn, midColumn, examColumn, totalColumn, gradeColumn, //positionColumn, 
	                            		 remarkColumn)
	                          
	                            .summary(cmp.verticalGap(20), cmp.horizontalList()
	                                .setGap(20)
	                                .add(  
	                                    cmp.text("Grade Details :").setStyle(titleStyle3).setHorizontalTextAlignment(HorizontalTextAlignment.LEFT)
//	                                    cmp.text("Number of Subjects : " + 10).setStyle(titleStyle3).setHorizontalTextAlignment(HorizontalTextAlignment.RIGHT)
	                                )
	                                
	                            )
	                            
	                            .summary(cmp.verticalGap(10), cmp.horizontalList()
	                                .add(  
	                                    cmp.text("A1 = 90 - 100, A2 = 80 - 89, A3 = 70 - 79, C = 50 - 69, P = 40 - 49, F = 0 - 39").setStyle(titleStyle3).setHorizontalTextAlignment(HorizontalTextAlignment.LEFT)
	                                    
	                                )
	                                
	                            )
	                            
	                            .summary(cmp.verticalGap(20), cmp.horizontalList().add(
	                                cmp.subreport(new JasperReportBuilder()
	                                        .setColumnTitleStyle(columnTitleStyle)
//	                                        .setColumnStyle(columnStyle)
	                                        .columns(affectColumn, ratingColumn)
	                                        .setDataSource(sub1())),
	                                cmp.subreport(new JasperReportBuilder()
	                                        .setColumnTitleStyle(columnTitleStyle)
//	                                        .setColumnStyle(columnStyle)
	                                        .columns(psychColumn, rateColumn)
	                                        .setDataSource(sub2())))   
	                                .setGap(20)
	                            )
	                            
	                            .summary(cmp.verticalGap(10), cmp.horizontalList()
	                                .add(  
	                                    cmp.text("SCALE : 5 = Excellent, 4 = Very Good, 3 = Good, 2 = Fair, 1 = Poor").setStyle(titleStyle3).setHorizontalTextAlignment(HorizontalTextAlignment.LEFT)
	                                    
	                                )
	                                
	                            )
	                            
	                            .summary(//Form Teacher Name
	                                    cmp.verticalGap(10), cmp.horizontalList()
	                                .add(  
	                                    cmp.text("Form Teacher:    " + getFormTeacher(std)).setStyle(titleStyle3).setHorizontalTextAlignment(HorizontalTextAlignment.LEFT)
	                                    
	                                )
	                                
	                            )
	                            
	                            .summary(//Form Teacher Remark
	                                    cmp.verticalGap(10), cmp.horizontalList()
	                                .add(  
	                                    cmp.text("Form Teacher Remark:    Good Result, put more Effort").setStyle(titleStyle3).setHorizontalTextAlignment(HorizontalTextAlignment.LEFT)
	                                    
	                                )
	                                
	                            )
	                            
	                            .summary(//Head Mistress Remark
	                                    cmp.verticalGap(10), cmp.horizontalList()
	                                .add(  
	                                    cmp.text("Head Mistress Remark:    Fair Performance").setStyle(titleStyle3).setHorizontalTextAlignment(HorizontalTextAlignment.LEFT)
	                                    
	                                )
	                                
	                            )
	                            
//	                            .summary(//Next Term Date
//	                                    cmp.verticalGap(10), cmp.horizontalList()
//	                                .add(  
//	                                    cmp.text("Next Term Begins:    2017-01-19").setStyle(titleStyle3).setHorizontalTextAlignment(HorizontalTextAlignment.LEFT)
//	                                    
//	                                )
//	                                
//	                            )
	                           
	                            .setDataSource(createDataSource()).print()//set datasource
	                            .setPageMargin(DynamicReports.margin().setLeft(30).setTop(30).setRight(30).setBottom(30));
	                            
//                                    JRPdfExporter exporter = new JRPdfExporter();
//                                    exporter.setExporterInput(new SimpleExporterInput(report.toJasperPrint()));
//                                    exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(out));
//                                    exporter.exportReport();
	                            
			} catch (Exception e) {
				e.printStackTrace();
			}
                        
                        return out;
		}
	        
	        //This is for setting N as a Naira Symbol and also to set Currency pattern
		private class CurrencyType extends BigDecimalType {
			private static final long serialVersionUID = 1L;

			@Override
			public String getPattern() {
				return "#,###";
			}
		}
	        
	        //This is where the data is populated or generated
	        //Though they have a container component to connect to database as
	        //as a datasource but i still never grab that one yet.
		private JRDataSource createDataSource() {
			DRDataSource dataSource = new DRDataSource("subject", "cw", "hw", "mid", "exam", "grade", "pos", "remark");
			
			List<Score> new_score = scores;
			List<String> mama = new ArrayList<>();

			HashMap<String, Score> mergedScore = new HashMap<String, Score>();
			for(Score product : scores){
			    if(mergedScore.containsKey(product.getSubject())&&(!mama.contains(product.getSubject()))){
			        Score existingScore = mergedScore.get(product.getSubject());
			        	for(Score pp: scores){
			        		if(existingScore.getSubject().contentEquals(pp.getSubject())){
			        			
			        			existingScore.setSubject(pp.getSubject());
						        existingScore.setCw(pp.getCw() + existingScore.getCw());
						        existingScore.setHw(pp.getHw() + existingScore.getHw());
						        existingScore.setMid(pp.getMid() + existingScore.getMid());
						        existingScore.setExam(pp.getExam() + existingScore.getExam());
			        		}
			        	}
			        
			        	mama.add(existingScore.getSubject());
			        	dataSource.add(existingScore.getSubject(), existingScore.getCw(), existingScore.getHw(), 
			        		existingScore.getMid(), existingScore.getExam(), getGrade(existingScore.getMid(), 
			        		existingScore.getExam()), 0, remark());
			        	rows++;
			        
			    } else {
			        mergedScore.put(product.getSubject(), product);
			    }
			}
			      
			return dataSource;
		}
	        
	        private JRDataSource sub1() {
			DRDataSource dataSource = new DRDataSource("affect", "rate");
			dataSource.add("English Language", "1");
	                dataSource.add("Punctuality", "1");
	                dataSource.add("Neatness", "3");
	                dataSource.add("Class Attention", "4");
	                dataSource.add("Class Participation", "4");
	                dataSource.add("Friendliness", "4");
	                dataSource.add("Honesty", "3");
	                dataSource.add("Self Control", "3");
	                dataSource.add("Politeness", "3");
			
	                
				return dataSource;
			}
	        
	        private JRDataSource sub2() {
				DRDataSource dataSource = new DRDataSource("psych", "rat");
				dataSource.add("Hand Writing", "1");
		                dataSource.add("Games/Sports", "1");
		                dataSource.add("Neatness", "3");
		                dataSource.add("Oratory", "4");
		                dataSource.add("Creativity", "4");
				
					return dataSource;
				}

	        private BufferedImage createImageFromBytes(byte[] imageData) {
        	    ByteArrayInputStream bais = new ByteArrayInputStream(imageData);
        	    try {
        	        return ImageIO.read(bais);
        	    } catch (IOException e) {
        	        throw new RuntimeException(e);
        	    }
        	}
	        
	        private String schoolLogo(){
	        	String logo = "";
	    		try {
	    			c = new Controller();
	    	    	
	    	    	String tenant = (String) VaadinSession.getCurrent().getAttribute("tenantID");
	    	    	JSONObject o = new JSONObject();
	    			o.put("tenantID", tenant);
	    			String response = c.postRequest("admin/settingid", o);
	    			if(response != null){
	    				
	    				JSONObject jp = new JSONObject(response);
	    				JSONObject j = jp.getJSONObject("response");
	    				
	    				logo = j.getString("logo");
	    			}
	    		} catch (JSONException e) {
	    			e.printStackTrace();
	    		}
	    		
	        	return logo;
	        }
	        
	        private String getStudentInfo(Student std){
	        	
	        	System.out.println("Printing Info : "  + std.toString());
	        	String pics = "";
	        	try {
	    			c = new Controller();
	    	    	
	    	    	String tenant = (String) VaadinSession.getCurrent().getAttribute("tenantID");
	    	    	JSONObject o = new JSONObject();
	    			o.put("tenantID", tenant);
	    			
	    			String response = c.postRequest("admin/allstudent", o);
	    			if(response != null){
	    				
	    				JSONObject jp = new JSONObject(response);
	    				JSONArray aj = jp.getJSONArray("response");
	    				
	    				for(int i = 0; i < aj.length(); i++){
	    					user = new Student();
	    					JSONObject j = aj.getJSONObject(i);
	    					
	    					if(j.getString("surname").equalsIgnoreCase(std.getSurname()) && j.getString("middle").equalsIgnoreCase(std.getMiddle()) && j.getString("other").equalsIgnoreCase(std.getOther())){
	    						
	    						names = j.getString("surname") + " " + j.getString("middle") + " " + j.getString("other");
	    						regno = j.getString("regno");
	    						klass = j.getString("klass");
	    						pic = j.getString("logo");
	    						
	    						
	    					}
	    				}
	    			}
	    		} catch (JSONException e) {
	    			e.printStackTrace();
	    		}
	        	return pics;
	        }
	        
	        private List<Score> pullResults(Student std, String term, String session) throws JSONException {
	        	
	        	List<Score> scores = new ArrayList<>();
	    		c = new Controller();
	    		JSONObject o = new JSONObject();
	    		o.put("tenantID", VaadinSession.getCurrent().getAttribute("tenantID"));
	    		o.put("klass", std.getKlass());
	    		o.put("term", term);
	    		o.put("session", session);
	    		String response = c.postRequest("admin/reportsheet", o);
	    		
	    		System.out.println("Printing Results : " + response);
	    		
	    		if(response != null){
	    					
	    			JSONObject jp = new JSONObject(response);
	    			JSONArray aj = jp.getJSONArray("response");
	    			
	    			for(int i = 0; i < aj.length(); i++){
	    				score = new Score();
	    				JSONObject a = aj.getJSONObject(i);
	    				
	    				score.setSubject(a.getString("subject"));

	    				
	    				if(a.getString("assess").equalsIgnoreCase("c/w")){
    						JSONArray b = a.getJSONArray("mark");
    						for(int s = 0; s < b.length(); s++){
    							
    	    					JSONObject n = b.getJSONObject(s);
    	    					if(n.getString("name").contains(std.getSurname())){
    	    						score.setCw(Integer.parseInt(n.getString("score")));
    	    					}
    						}
						}
	    				if(a.getString("assess").equalsIgnoreCase("h/w")){
    						JSONArray b = a.getJSONArray("mark");
    						for(int s = 0; s < b.length(); s++){
    	    					JSONObject n = b.getJSONObject(s);
    	    					if(n.getString("name").contains(std.getSurname())){
    	    						score.setHw(Integer.parseInt(n.getString("score")));
    	    					}
    						}
						}
	    				if(a.getString("assess").equalsIgnoreCase("mid")){
    						JSONArray b = a.getJSONArray("mark");
    						for(int s = 0; s < b.length(); s++){
    	    					JSONObject n = b.getJSONObject(s);
    	    					if(n.getString("name").contains(std.getSurname())){
    	    						score.setMid(Integer.parseInt(n.getString("score")));
    	    					}
    						}
						}
	    				if(a.getString("assess").equalsIgnoreCase("exam")){
    						JSONArray b = a.getJSONArray("mark");
    						for(int s = 0; s < b.length(); s++){
    	    					JSONObject n = b.getJSONObject(s);
    	    					if(n.getString("name").contains(std.getSurname())){
    	    						score.setExam(Integer.parseInt(n.getString("score")));
    	    					}
    						}
						}
	    			
    				scores.add(score);
	    			}
	    			System.out.println("Printing Result : "+ scores);
	    		}
				return scores;
	        }

	        private String getGrade(int mid, int exam){
	        	grade = "";
	        	
	        	int total = mid + exam;
	        	
	        	if(total >= 90 && !(total > 100)){
	        		grade = "A1";
	        	}
	        	else if(total >= 80 && !(total > 89)){
	        		grade = "A2";
	        	}
	        	else if(total >= 70 && !(total > 79)){
	        		grade = "A3";
	        	}
	        	else if(total >= 50 && !(total > 69)){
	        		grade = "C";
	        	}
	        	else if(total >= 40 && !(total > 49)){
	        		grade = "P";
	        	}
	        	else {
	        		grade = "F";
	        	}
	        	
	        	return grade;
	        	
	        }

	        private int getPosition(Student selected, String term, String session){
	        	int p = new ClassPosition().getPosition(selected, term, session);
	        	return p;
	        }

	        private int getNumberInClass(Student selected){
	        	int p = new ClassPosition().getNumberInClass(selected);
	        	return p;
	        }
	        
	        private String remark(){
	        	
	        	remark = "";
	        	
	        	if(grade.equals("A1")){
	        		remark = "Excellent";
	        	}
	        	else if(grade.equals("A2")){
	        		remark = "Very Good";
	        	}
	        	else if(grade.equals("A3")){
	        		remark = "Good";
	        	}
	        	else if(grade.equals("C")){
	        		remark = "Fair";
	        	}
	        	else {
	        		remark = "Poor";
	        	}
	        	
	        	return remark;
	        }

	        private String getFormTeacher(Student std){
	        	
	        	String name = "";
	        	
	        	c = new Controller();
	    		JSONObject o = new JSONObject();
	    		try {
	    			o.put("tenantID", VaadinSession.getCurrent().getAttribute("tenantID"));
	    			String response = c.postRequest("admin/classes", o);
	    			
	    			if(response != null){
	    				
	    				JSONObject jp = new JSONObject(response);
	    				JSONArray aj = jp.getJSONArray("response");
	    				
	    				for(int i = 0; i < aj.length(); i++){
	    					JSONObject j = aj.getJSONObject(i);
	    					if(std.getKlass().equals(j.get("klassName"))){
	    						name = j.getString("teacher");
	    					}
	    				}
	    			}
	    		}
	    		catch(Exception e){
	    			e.printStackTrace();
	    		}
	        	
	        	return name;
	        }
}