package nova.academia.util;

import com.vaadin.server.Page;
import com.vaadin.shared.Position;
import com.vaadin.ui.Notification;

public class Notify {

	public static void notify(String message){
		Notification notification = new Notification(message);
//        notification.setDescription("<span>AcademiaSMS is an All in One School Administeration and Learning Management System. An EASY to use platform engineered by <a href=\"https://novasys.com\">NOVA SYSTEMS</a>.</span> <span>Your username and password is required to login into the Platform, then click the <b>Sign In</b> button to continue.</span>");
        notification.setHtmlContentAllowed(true);
        notification.setStyleName("tray dark small closable login-help");
        notification.setPosition(Position.MIDDLE_CENTER);
//        notification.setDelayMsec(20000);
        notification.show(Page.getCurrent());
	}
}
