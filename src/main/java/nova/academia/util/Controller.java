package nova.academia.util;

import org.json.JSONObject;

public class Controller {

	private HttpUtil nav = new HttpUtil();

    public String getResponse(String url){
    	String data = nav.getService(url);
    	return data;
    }
    
    public String postRequest(String url, JSONObject json){
    	String data = nav.postService(url, json);
    	return data;
    }
}
