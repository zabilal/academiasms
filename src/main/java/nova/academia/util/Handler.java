package nova.academia.util;

import java.io.Serializable;

import com.vaadin.event.Action;

public interface Handler extends Serializable {
	
	public Action[] getActions(Object target, Object sender);
	
	public void handleAction(Action action, Object sender, Object target);
}
