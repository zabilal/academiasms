package nova.academia.util;

import java.io.IOException;
import static org.apache.http.HttpHeaders.USER_AGENT;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;

import com.vaadin.sass.internal.parser.ParseException;


/**
 *
 * @author nova
 */
public class HttpUtil {

    private String result;
    private HttpResponse response;
    private HttpClient httpClient = HttpClientBuilder.create().build(); 
    private JSONObject res;

    private final String BASE_URL = "http://0.0.0.0:8093/";   //Production
    
    public final String OP_URL = BASE_URL+"op/";
    
    public HttpUtil() {
       
    }


    public String postService(String url, JSONObject json){
        
    	String final_url = BASE_URL + url;
        try {
            HttpPost request = new HttpPost(final_url);
            StringEntity params = new StringEntity(json.toString());
            request.addHeader("content-type", "application/json");
            request.addHeader("Accept","application/json");
            request.setEntity(params);
            response = httpClient.execute(request);
            System.out.println("Passing through httpClient.execute()");
            
            // handle response here...lng
            if (response != null) {
                
                // CONVERT RESPONSE TO STRING
                result = EntityUtils.toString(response.getEntity());
                System.out.println("result " + result);
            }
        } catch (IOException | ParseException ex){
                ex.printStackTrace();
            }
        return result;
    }
    
    public String getService(String url){
        
        try {
        	String final_url = BASE_URL + url;
            HttpClient client = HttpClientBuilder.create().build();
            HttpGet request = new HttpGet(final_url);
            
            // add request header
            request.addHeader("User-Agent", USER_AGENT);
            HttpResponse response = client.execute(request);
            
            result = EntityUtils.toString(response.getEntity());
                System.out.println("result " + result);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return result;
    }
}