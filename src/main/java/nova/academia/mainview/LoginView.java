package nova.academia.mainview;

import org.json.JSONException;
import org.json.JSONObject;

import com.vaadin.event.ShortcutAction.KeyCode;
import com.vaadin.server.FontAwesome;
import com.vaadin.server.Page;
import com.vaadin.server.Responsive;
import com.vaadin.server.ThemeResource;
import com.vaadin.server.VaadinSession;
import com.vaadin.shared.Position;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.Component;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Image;
import com.vaadin.ui.Label;
import com.vaadin.ui.Notification;
import com.vaadin.ui.PasswordField;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import com.vaadin.ui.themes.ValoTheme;

import nova.academia.settings.view.Register;
import nova.academia.settings.view.Users;
import nova.academia.util.Controller;
import nova.academia.util.Notify;
import nova.academia.util.Storage;

@SuppressWarnings("serial")
public class LoginView extends VerticalLayout {

	private Controller c;
	
    public LoginView() {
        setSizeFull();
        c = new Controller();
        
        Component loginForm = buildLoginForm();
        addComponent(loginForm);
        setComponentAlignment(loginForm, Alignment.MIDDLE_CENTER);
        
        addStyleName("login-cover");

        Notification notification = new Notification("Welcome to Academia School Management System");
        notification.setDescription("<span>AcademiaSMS is an All in One School Administeration and Learning Management System. An EASY to use platform engineered by <a href=\"https://novasys.com\">NOVA SYSTEMS</a>.</span> <span>Your username and password is required to login into the Platform, then click the <b>Sign In</b> button to continue.</span>");
        notification.setHtmlContentAllowed(true);
        notification.setStyleName("tray dark small closable login-help");
        notification.setPosition(Position.BOTTOM_CENTER);
        notification.setDelayMsec(20000);
        notification.show(Page.getCurrent());
    }

    private Component buildLoginForm() {
        final VerticalLayout loginPanel = new VerticalLayout();
        loginPanel.setSizeUndefined();
        loginPanel.setSpacing(true);
        Responsive.makeResponsive(loginPanel);
        loginPanel.addStyleName("login-panel");

        final Component image  = buildImage();
        loginPanel.addComponent(image);
        loginPanel.setComponentAlignment(image, Alignment.TOP_CENTER);
        
        loginPanel.addComponent(buildLabels());
        loginPanel.addComponent(buildFields());
        
        final HorizontalLayout lay = new HorizontalLayout();
        lay.addComponent(new CheckBox("Remember me", true));
        lay.setWidth("80%");
        Button acc = new Button("Create Account");
        acc.addStyleName(ValoTheme.BUTTON_LINK);
        acc.addStyleName(ValoTheme.BUTTON_BORDERLESS);
        acc.setHeight("30px");
        
        acc.addClickListener(event -> {
			Window win = new Window();
			win.setContent(new Register());
			win.setModal(true);
			win.setClosable(true);
			this.getUI().addWindow(win);
		});
        
        lay.addComponent(acc);
        loginPanel.addComponent(lay);
        
        return loginPanel;
    }

    private Component buildFields() {
        VerticalLayout fields = new VerticalLayout();
        fields.setSpacing(true);
        fields.setMargin(true);
        fields.addStyleName("fields");
             
        final TextField username = new TextField();
        username.setInputPrompt("Username");
        username.setIcon(FontAwesome.USER);
        username.addStyleName(ValoTheme.TEXTFIELD_INLINE_ICON);

        final PasswordField password = new PasswordField();
        password.setInputPrompt("Password" );
        password.setIcon(FontAwesome.LOCK);
        password.addStyleName(ValoTheme.TEXTFIELD_INLINE_ICON);

        final Button signin = new Button("Sign In");
        signin.addStyleName(ValoTheme.BUTTON_PRIMARY);
        signin.setClickShortcut(KeyCode.ENTER);
        signin.focus();

        fields.addComponents(username, password, signin);
        fields.setComponentAlignment(signin, Alignment.BOTTOM_LEFT);

        signin.addClickListener(new ClickListener() {
            @Override
            public void buttonClick(final ClickEvent event) {
            	try {
					loginAttempt();
				} catch (JSONException e) {
					e.printStackTrace();
				}
            }

			private void loginAttempt() throws JSONException {
				JSONObject log = new JSONObject();
				log.put("username", username.getValue());
				log.put("password", password.getValue());
				String l = c.postRequest("admin/interlog", log);
				try{
					JSONObject oj = new JSONObject(l);
					JSONObject j = oj.getJSONObject("response");
					if(oj.getString("response") != null){
						VaadinSession.getCurrent().setAttribute("tenantID", j.getString("tenantID"));
						VaadinSession.getCurrent().setAttribute("userID", j.getString("userID"));
						VaadinSession.getCurrent().setAttribute("role", j.getString("role"));
						VaadinSession.getCurrent().setAttribute("fullname", j.getString("fullname"));
						getParent().getUI().setContent(new MainView());
					}
				}catch(Exception e){
						Notify.notify("Invalid User Credentials");
				}
			}
        });
        return fields;
    }

    private Component buildLabels() {
        CssLayout labels = new CssLayout();
        labels.addStyleName("labels");
        
        Label welcome = new Label("Welcome");
        welcome.setSizeUndefined();
        welcome.addStyleName(ValoTheme.LABEL_H4);
        welcome.addStyleName(ValoTheme.LABEL_COLORED);
//        labels.addComponent(welcome);

        Label title = new Label("Academia School Management System");
        title.setSizeUndefined();
        Responsive.makeResponsive(title);
        title.addStyleName(ValoTheme.LABEL_H3);
        title.addStyleName(ValoTheme.LABEL_LIGHT);
        labels.addComponent(title);
        return labels;
    }
    
    private Component buildImage(){
    	
    	Image imageFromTheme = new Image(null, new ThemeResource(
        		"img/icon-magazine.png"));
    	imageFromTheme.setHeight("100px");
    	imageFromTheme.setWidth("100px");
    	
    	return imageFromTheme;
    }

}
