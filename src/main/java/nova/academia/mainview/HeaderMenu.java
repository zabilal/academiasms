package nova.academia.mainview;

import com.vaadin.server.Responsive;
import com.vaadin.server.ThemeResource;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Component;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;

public class HeaderMenu extends HorizontalLayout{
	
	private Label title, logo;
	
	public HeaderMenu(){
		
		Responsive.makeResponsive(this);
		setWidth("100%");
		setPrimaryStyleName("header");
		
		addComponent(buildHeader());
	}
	
	public Component buildHeader(){
		
		HorizontalLayout lay = new HorizontalLayout();
		Responsive.makeResponsive(lay);
    	lay.setWidthUndefined();
    	lay.setHeight("70px");
		
//		title = new Label("<h1><b>Al-Mubarrak Al-Mutakamila Schools</b></h1>",
//                ContentMode.HTML);
//		title.setStyleName("label-title");
//		Responsive.makeResponsive(title);
		
		logo = new Label();
		logo.setHeight("70%");
    	logo.setIcon(new ThemeResource("favicon.ico"));
    	
//    	lay.addComponent(logo);
//    	lay.setComponentAlignment(logo, Alignment.MIDDLE_LEFT);
		
		lay.addComponent(title);
		lay.setComponentAlignment(title, Alignment.MIDDLE_CENTER);
		
		return lay;
	}
	

	/////////////////////////////////////////////////////////////////////////////////////////////////////////
	public Label getTitle() {
		return title;
	}

	public void setTitle(Label title) {
		this.title = title;
	}
	
	

}
