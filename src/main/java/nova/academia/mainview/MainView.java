package nova.academia.mainview;

import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.imageio.ImageIO;

import org.apache.commons.codec.binary.Base64;
import org.json.JSONException;
import org.json.JSONObject;

import com.vaadin.icons.VaadinIcons;
import com.vaadin.server.FontAwesome;
import com.vaadin.server.Page;
import com.vaadin.server.Resource;
import com.vaadin.server.Responsive;
import com.vaadin.server.StreamResource;
import com.vaadin.server.StreamResource.StreamSource;
import com.vaadin.server.ThemeResource;
import com.vaadin.server.VaadinService;
import com.vaadin.server.VaadinSession;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Notification.Type;
import com.vaadin.ui.VerticalLayout;

import nova.academia.academic.GradeView;
import nova.academia.academic.SessionView;
import nova.academia.academic.SubjectView;
import nova.academia.academic.TermView;
import nova.academia.academic.klassView;
import nova.academia.admission.ParentView;
import nova.academia.admission.Student2View;
import nova.academia.admission.StudentView;
import nova.academia.assessment.AssessView;
import nova.academia.assessment.MarkReport;
import nova.academia.assessment.Marker;
import nova.academia.assessment.MarkerView;
import nova.academia.employee.Teacher;
import nova.academia.nav.SideMenu;
import nova.academia.nav.SideMenu.Tabber;
import nova.academia.reports.ReportSheet;
import nova.academia.settings.view.Gateway;
import nova.academia.settings.view.Setup;
import nova.academia.settings.view.Users;
import nova.academia.util.Controller;
import nova.academia.util.Notify;

/*
 * Dashboard MainView is a simple HorizontalLayout that wraps the menu on the
 * left and creates a simple container for the navigator on the right.
 */
@SuppressWarnings("serial")
public class MainView extends HorizontalLayout {

	private SideMenu sideMenu = new SideMenu();
	
	private boolean logoVisible = true;
//	private ThemeResource logo = new ThemeResource("icon.jpg");
	private String menuCaption = "RAJI ZAKARIYYA";
	private Controller c;
	
	
    public MainView() {
    	
    	Responsive.makeResponsive(this);
        setSizeFull();
        addStyleName("mainview");
        
        String logo = schoolLogo();
        byte[] logos = Base64.decodeBase64(logo);
        
        StreamResource resource = new StreamResource(
    	        new StreamResource.StreamSource() {
    	            @Override
    	            public InputStream getStream() {
    	                 try {
    	                  BufferedImage bi = ImageIO.read(new ByteArrayInputStream(logos));
    	                  
    	                  int scaleX = (int) (bi.getWidth() * 0.5);
    	                  int scaleY = (int) (bi.getHeight() * 0.5);
    	                  
    	                  java.awt.Image newImg = bi.getScaledInstance(scaleX, scaleY, java.awt.Image.SCALE_SMOOTH);
    	                  /* Write the image to a buffer. */
    	                  ByteArrayOutputStream imagebuffer = new ByteArrayOutputStream();
    	                  BufferedImage bimage = new BufferedImage(scaleX, scaleY, BufferedImage.TYPE_INT_RGB);
    	                  Graphics bg = bimage.getGraphics();
    	                  bg.drawImage(newImg, 0, 0, null);
    	                  bg.dispose();
    	                  ImageIO.write(bimage, "jpg", imagebuffer);
    	                  
    	                  /* Return a stream from the buffer. */
    	                  return new ByteArrayInputStream(imagebuffer.toByteArray());//logos);
    	                } catch (IOException e) {
    	                    return null;
    	                }
    	            }
    	        }, "filename.png");
    	    
//    	    image.setSource(resource);
        
    	sideMenu.setMenuCaption((String)VaadinSession.getCurrent().getAttribute("fullname"), resource);

    	setUser("Account", null);
    	
    	buildMenu();
    		
        addComponent(sideMenu);
        
        Label title = new Label(schoolName());
        title.setPrimaryStyleName("school-title");
        title.setSizeFull();
        sideMenu.setHeaderContent(title);
    }
    
     

    private void setUser(String name, Resource icon) {
		sideMenu.setUserName(name);
		sideMenu.setUserIcon(icon);

		sideMenu.clearUserMenu();
		sideMenu.addUserMenuItem("Settings", FontAwesome.WRENCH, () -> {
			Notification.show("Showing settings", Type.TRAY_NOTIFICATION);
		});
		sideMenu.addUserMenuItem("Log out", () -> {
			Notification.show("Logging out..", Type.TRAY_NOTIFICATION);
			getUI().getSession().close();
			Page.getCurrent().reload();//Better option than below
//			getUI().setContent(new LoginView());
		});

//		sideMenu.addUserMenuItem("Hide logo", () -> {
//			if (!logoVisible) {
//				sideMenu.setMenuCaption(menuCaption, logo);
//			} else {
//				sideMenu.setMenuCaption(menuCaption);
//			}
//			logoVisible = !logoVisible;
//		});
	}
	
  //Menu Tab creation
    
    private void buildMenu(){
    	
    	
		//sideMenu.addTab("", null);
		
		Tabber home = sideMenu.addTab("Dashboard", FontAwesome.DASHBOARD);
		home.addButton("Dashboard", () -> {
		    	//Action Here
			Notify.notify("Next Update !!!!");
		});
		//////////////////////////////////////////////////////////
		Tabber admit = sideMenu.addTab("Admissions", FontAwesome.FILE_ARCHIVE_O);
		admit.addButton("New Student", () -> {
			//Action Here
			sideMenu.setContent(new StudentView());
		});
		admit.addButton("All Applicants", () -> {
			//Action Here
			Notify.notify("Next Update !!!!");
		});
		//////////////////////////////////////////////////////////
		Tabber people = sideMenu.addTab("People", FontAwesome.USERS);
		people.addButton("Students", () -> {
		    	//Action Here
			sideMenu.setContent(new Student2View());
		});
		people.addButton("Parents/Guardians", () -> {
	    	//Action Here
			sideMenu.setContent(new ParentView());
		});
		//////////////////////////////////////////////////////////
		Tabber library = sideMenu.addTab("Library", FontAwesome.BOOK);
//		library.addButton("Manage Books", () -> {
//	    	//Action Here
//			Notify.notify("Next Update !!!!");
//		});
//		library.addButton("Book Categories", () -> {
//    	//Action Here
//			Notify.notify("Next Update !!!!");
//		});
//		library.addButton("Issue Books", () -> {
//			//Action Here
//			Notify.notify("Next Update !!!!");
//		});
//		library.addButton("Return Books", () -> {
//			//Action Here
//			Notify.notify("Next Update !!!!");
//		});
//		library.addButton("Issue History", () -> {
//	    	//Action Here
//			Notify.notify("Next Update !!!!");
//		});
		//////////////////////////////////////////////////////////
		Tabber doc = sideMenu.addTab("Files & Cabinet", FontAwesome.FILE_ARCHIVE_O);
//		doc.addButton("All Cabinet", () -> {
//			//Action Here
//			Notify.notify("Next Update !!!!");
//		});
//		doc.addButton("Files & Documents", () -> {
//			//Action Here
//			Notify.notify("Next Update !!!!");
//		});
		//////////////////////////////////////////////////////////
		Tabber plan = sideMenu.addTab("Academic Planning", FontAwesome.INSTITUTION);
		plan.addButton("Manage Subjects", () -> {
	    	//Action Here
			sideMenu.setContent(new SubjectView());
		});
		plan.addButton("Manage Grades", () -> {
	    	//Action Here
			sideMenu.setContent(new GradeView());
		});
		plan.addButton("Manage Classes", () -> {
	    	//Action Here
			sideMenu.setContent(new klassView());
		});
		plan.addButton("Manage Sessions", () -> {
	    	//Action Here
			sideMenu.setContent(new SessionView());
		});
		plan.addButton("Manage Terms", () -> {
	    	//Action Here
			sideMenu.setContent(new TermView());
		});
		plan.addButton("Class Attendance", () -> {
	    	//Action Here
			Notify.notify("Next Update !!!!");
		});
		plan.addButton("Time Table", () -> {
	    	//Action Here
			Notify.notify("Next Update !!!!");
		});
		plan.addButton("Calender/Schedules", () -> {
	    	//Action Here
			Notify.notify("Next Update !!!!");
		});
		//////////////////////////////////////////////////////////
		Tabber fees = sideMenu.addTab("Fees", FontAwesome.MONEY);
		fees.addButton("All Fees", () -> {
	    	//Action Here
			Notify.notify("Next Update !!!!");
		});
		fees.addButton("Make Payment", () -> {
	    	//Action Here
			Notify.notify("Next Update !!!!");
		});
		fees.addButton("Fee Payment Report", () -> {
	    	//Action Here
			Notify.notify("Next Update !!!!");
		});
		//////////////////////////////////////////////////////////
		Tabber exam = sideMenu.addTab("Assessments & Reports", FontAwesome.HISTORY);
		exam.addButton("Assessment", () -> {
			//Action Here
			sideMenu.setContent(new AssessView());
		});
		exam.addButton("Report Sheet", () -> {
			//Action Here
			sideMenu.setContent(new ReportSheet());
		});
		exam.addButton("Broad Sheet", () -> {
			//Action Here
			Notify.notify("Next Update !!!!");
		});
		exam.addButton("Promote Students", () -> {
			//Action Here
			Notify.notify("Next Update !!!!");
		});
		//////////////////////////////////////////////////////////
//		Tabber pic = sideMenu.addTab("Photo Gallery", FontAwesome.PICTURE_O);
//		pic.addButton("Create Gallery", () -> {
//			//Action Here
//			Notify.notify("Next Update !!!!");
//		});
//		pic.addButton("Pictures", () -> {
//			//Action Here
//			Notify.notify("Next Update !!!!");
//		});
		//////////////////////////////////////////////////////////
		Tabber store = sideMenu.addTab("Store & Inventory", FontAwesome.SHOPPING_BASKET);
//		store.addButton("Create Item", () -> {
//			//Action Here
//			Notify.notify("Next Update !!!!");
//		});
//		store.addButton("Pushout Item", () -> {
//			//Action Here
//			Notify.notify("Next Update !!!!");
//		});
//		store.addButton("Inventory Report", () -> {
//			//Action Here
//			Notify.notify("Next Update !!!!");
//		});
		//////////////////////////////////////////////////////////
//		Tabber hostel = sideMenu.addTab("Hostel & Accomodation", FontAwesome.HOTEL);
//		hostel.addButton("Hostel Details", () -> {
//	    	//Action Here
//			Notify.notify("Next Update !!!!");
//		});
//		hostel.addButton("Hostel Rooms", () -> {
//	    	//Action Here
//			Notify.notify("Next Update !!!!");
//		});
//		hostel.addButton("Hostel Allocation", () -> {
//	    	//Action Here
//			Notify.notify("Next Update !!!!");
//		});
//		hostel.addButton("Request Details", () -> {
//	    	//Action Here
//			Notify.notify("Next Update !!!!");
//		});
//		hostel.addButton("Hostel Transfer/Vacate", () -> {
//	    	//Action Here
//			Notify.notify("Next Update !!!!");
//		});
//		hostel.addButton("Hostel Register", () -> {
//	    	//Action Here
//			Notify.notify("Next Update !!!!");
//		});
//		hostel.addButton("Hostel Visitors", () -> {
//	    	//Action Here
//			Notify.notify("Next Update !!!!");
//		});
//		hostel.addButton("Hostel Fee Collection", () -> {
//	    	//Action Here
//			Notify.notify("Next Update !!!!");
//		});
//		hostel.addButton("Hostel Report", () -> {
//	    	//Action Here
//			Notify.notify("Next Update !!!!");
//		});
		//////////////////////////////////////////////////////////
		Tabber book = sideMenu.addTab("School Year Book", FontAwesome.FILE_PICTURE_O);
//		book.addButton("Create Year Book", () -> {
//			//Action Here
//			Notify.notify("Next Update !!!!");
//		});
//		book.addButton("Manage Year Book", () -> {
//			//Action Here
//			Notify.notify("Next Update !!!!");
//		});
		//////////////////////////////////////////////////////////
		Tabber account = sideMenu.addTab("Finance Tools", FontAwesome.USERS);
		account.addButton("Income & Expense", () -> {
			//Action Here
			Notify.notify("Next Update !!!!");
		});
		account.addButton("Transaction", () -> {
			//Action Here
			Notify.notify("Next Update !!!!");
		});
		account.addButton("Payment Notification", () -> {
			//Action Here
			Notify.notify("Next Update !!!!");
		});
		account.addButton("Cash Flow Report", () -> {
			//Action Here
			Notify.notify("Next Update !!!!");
		});
		//////////////////////////////////////////////////////////
		Tabber emp = sideMenu.addTab("Employee Management", FontAwesome.INSTITUTION);
		emp.addButton("Add Staff", () -> {
			//Action Here
			Notify.notify("Next Update !!!!");
		});
		emp.addButton("Add Teachers", () -> {
			//Action Here
			sideMenu.setContent(new Teacher());
//			Notify.notify("Next Update !!!!");
		});
		emp.addButton("Manage Staff", () -> {
		//Action Here
			Notify.notify("Next Update !!!!");
		});
		emp.addButton("Manage Teachers", () -> {
		//Action Here
			Notify.notify("Next Update !!!!");
		});
//		emp.addButton("Manage Payroll", () -> {
//		//Action Here
//			Notify.notify("Next Update !!!!");
//		});
//		emp.addButton("Manage Payslip", () -> {
//		//Action Here
//			Notify.notify("Next Update !!!!");
//		});
		//////////////////////////////////////////////////////////
		Tabber setting = sideMenu.addTab("Tools & Settings", FontAwesome.GEARS);
//		setting.addButton("User Roles", () -> {
//	    	//Action Here
//			Notification.show("User Roles Button has been Clicked");
//		});		
//		setting.addButton("User Privileges", () -> {
//	    	//Action Here
//			Notification.show("User Privileges has been Clicked");
//		});
		setting.addButton("Manage Users", () -> {
	    	//Action Here
			sideMenu.setContent(new Users());
		});
//		setting.addButton("Payment Gateway", () -> {
//	    	//Action Here
//			sideMenu.setContent(new Gateway());
//		});
		setting.addButton("School Setup", () -> {
		    	//Action Here
    			sideMenu.setContent(new Setup());
		});
    	
    }
		
    private String schoolName(){
    	String title = "";
		try {
			c = new Controller();
	    	
	    	String tenant = (String) VaadinSession.getCurrent().getAttribute("tenantID");
	    	JSONObject o = new JSONObject();
			o.put("tenantID", tenant);
			String response = c.postRequest("admin/settingid", o);
			if(response != null){
				
				JSONObject jp = new JSONObject(response);
				JSONObject j = jp.getJSONObject("response");
				
				title = j.getString("schoolName");
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
    	return title;
    }
    
    private String schoolLogo(){
    	String logo = "";
		try {
			c = new Controller();
	    	
	    	String tenant = (String) VaadinSession.getCurrent().getAttribute("tenantID");
	    	JSONObject o = new JSONObject();
			o.put("tenantID", tenant);
			String response = c.postRequest("admin/settingid", o);
			if(response != null){
				
				JSONObject jp = new JSONObject(response);
				JSONObject j = jp.getJSONObject("response");
				
				logo = j.getString("logo");
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
    	return logo;
    }
    
    }
    
    

