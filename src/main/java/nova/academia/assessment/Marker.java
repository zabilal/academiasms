package nova.academia.assessment;

import java.util.ArrayList;
import java.util.Iterator;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.vaadin.addon.contextmenu.ContextMenu;
import com.vaadin.data.Property;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.server.VaadinSession;
import com.vaadin.ui.Button;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.Component;
import com.vaadin.ui.Grid;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.ValoTheme;
import com.vaadin.ui.Grid.SingleSelectionModel;
import nova.academia.util.Controller;
import nova.academia.util.Notify;

public class Marker extends VerticalLayout{
	
	private ComboBox klass;
	private ComboBox subject;
	private Label assess;
	private Label session;
	private Label term;
	private static boolean isEdit;
	private Controller c;
	private Mark user;
	private Assess ass;
	private BeanItemContainer<Mark> container;
	private Grid grid;
	
	private static ArrayList me;
	
	
	public Marker(){
		
		setPrimaryStyleName("material");
		addStyleName("setup-view");
//		setWidth("80%");
		setMargin(true);
		setSpacing(true);
		user = new Mark();
		
		me = new ArrayList<>();

		try {
			loadAssess();
		} catch (JSONException e2) {
			e2.printStackTrace();
		}
		
		
		Label title = new Label("Score Sheet");
		title.setPrimaryStyleName("view-title");
		addComponent(title);
		addComponent(buildCriteria());
		addComponent(buildTable());
		
		Button but = new Button("Save Scores", e -> {
			ArrayList<Mark> mark = new ArrayList<>();
			// Iterate over the item identifiers of the table.
			for (Iterator i = grid.getContainerDataSource().getItemIds().iterator(); i.hasNext();) {
			    // Get the current item identifier, which is an integer.
			    Mark m = (Mark) i.next();
			    
			    mark.add(m);
			    // Now get the actual item from the table.
//			    Item item = grid.getContainerDataSource().getItem(iid);

			}
			try {
				pushScore(mark);
			} catch (JSONException e1) {
				e1.printStackTrace();
			}
			
        });
		but.addStyleName(ValoTheme.BUTTON_FRIENDLY);
		addComponent(but);
		
		try {
			popCombo();
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}
	
	private Component buildTable(){
		
		grid = new Grid();
		grid.setWidth("1000px");
		grid.setResponsive(true);
		grid.setEditorEnabled(true);
		
		//Adding Columns to Grid
		grid.addColumn("Reg No", String.class);
		grid.addColumn("Name");
		for(int i = 0; i < me.size(); i++){
			System.out.println(me.get(i));
			grid.addColumn(me.get(i), String.class).setWidth(100);
		}

		
		return grid;
	}

	@SuppressWarnings("deprecation")
	private Component buildCriteria(){
		
		HorizontalLayout lay = new HorizontalLayout();
		lay.setSizeFull();
		
		subject = new ComboBox("Select Subject");
		subject.setWidth("40%");
		lay.addComponent(subject);
		
		klass = new ComboBox("Select Class");
		klass.setWidth("40%");
		lay.addComponent(klass);
		
		Property.ValueChangeListener listener = new Property.ValueChangeListener() {
		    public void valueChange(ValueChangeEvent event) {
		        try {
					loadStudent(klass.getValue().toString());
				} catch (JSONException e) {
					e.printStackTrace();
				}
		    }
		};
		klass.addListener(listener);
		
		return lay;
	}
	
	private void loadAssess() throws JSONException{
		c = new Controller();
		
		JSONObject o = new JSONObject();
		o.put("tenantID", VaadinSession.getCurrent().getAttribute("tenantID"));
		String response = c.postRequest("admin/assesses", o);
		
		if(response != null){
			
			JSONObject jp = new JSONObject(response);
			JSONArray aj = jp.getJSONArray("response");
			
			for(int i = 0; i < aj.length(); i++){
				ass = new Assess();
				JSONObject j = aj.getJSONObject(i);
				me.add(j.getString("assess"));
			}	
		}
		
	}

	private void loadStudent(String klass) throws JSONException{
		c = new Controller();
		
		JSONObject o = new JSONObject();
		o.put("tenantID", VaadinSession.getCurrent().getAttribute("tenantID"));
		String response = c.postRequest("admin/allstudent", o);
		
		if(response != null){
			
			JSONObject jp = new JSONObject(response);
			JSONArray aj = jp.getJSONArray("response");
			
			for(int i = 0; i < aj.length(); i++){
				user = new Mark();
				JSONObject j = aj.getJSONObject(i);
				if(klass.equals(j.getString("klass"))){
					user.setRegno(j.getString("regno"));
					String a = j.getString("surname");
					String b = j.getString("middle");
					String c = j.getString("other");
					user.setName(a + " " + b + " " + c);
//					user.setScore("0");
					grid.addRow(j.getString("regno"),  a + " " + b + " " + c, "0", "0", "0");
				}
			}

		}
	}
	
	private void popCombo() throws JSONException{
		c = new Controller();
		JSONObject o = new JSONObject();
		o.put("tenantID", VaadinSession.getCurrent().getAttribute("tenantID"));
		String response = c.postRequest("admin/subjects", o);
		String response2 = c.postRequest("admin/classes", o);
		
		if(response != null && response2 != null){
			
			JSONObject jp = new JSONObject(response);
			JSONArray aj = jp.getJSONArray("response");
			
			JSONObject ab = new JSONObject(response2);
			JSONArray b = ab.getJSONArray("response");
			
			for(int i = 0; i < b.length(); i++){
			
				JSONObject j = aj.getJSONObject(i);
				JSONObject k = b.getJSONObject(i);
				subject.addItem(j.getString("subjectName"));
				klass.addItem(k.getString("klassName"));
				
			}
				
		}
	}
	
	private void pushScore(ArrayList<Mark> mark) throws JSONException{
		c = new Controller();
				
		JSONObject json = new JSONObject();
		json.put("tenantID", VaadinSession.getCurrent().getAttribute("tenantID"));
		json.put("term", term.getValue());
		json.put("assess", assess.getValue());
		json.put("session", session.getValue());
		json.put("subject", subject.getValue());
		json.put("klass", klass.getValue());
		
		JSONArray array = new JSONArray();
		for (int i=0; i < mark.size(); i++) {
			JSONObject y = new JSONObject();
	        y.put("regno", mark.get(i).getRegno());
	        y.put("name", mark.get(i).getName());
	        y.put("score", mark.get(i).getScore());
	        array.put(y);
		}
		json.put("mark", array);
		System.out.println(json);
		
		String h = c.postRequest("admin/result", json);
		JSONObject oj = new JSONObject(h);
		JSONObject j = oj.getJSONObject("response");
		if(j != null){
			Notify.notify("Score is Saved");
		}else{
			Notify.notify("Score not Saved");
		}
	}
	
	
}
