package nova.academia.assessment;

import java.util.ArrayList;
import java.util.Iterator;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.vaadin.addon.contextmenu.ContextMenu;
import com.vaadin.data.Property;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.server.VaadinSession;
import com.vaadin.ui.Button;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.Component;
import com.vaadin.ui.Grid;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.ValoTheme;
import com.vaadin.ui.Grid.SingleSelectionModel;
import nova.academia.util.Controller;
import nova.academia.util.Notify;

public class MarkerView extends VerticalLayout{
	
	private ComboBox klass;
	private ComboBox subject;
	private Label assess;
	private Label session;
	private Label term;
	private static boolean isEdit;
	private Controller c;
	private Mark user;
	private Assess ass;
	private BeanItemContainer<Mark> container;
	private Grid grid;
	
	
	
	public static boolean isEdit() {
		return isEdit;
	}

	public static void setEdit(boolean isEdit) {
		MarkerView.isEdit = isEdit;
	}

	public MarkerView(Assess assess){
		
		setPrimaryStyleName("material");
		addStyleName("setup-view");
//		setWidth("80%");
		setMargin(true);
		setSpacing(true);
		user = new Mark();
		user.setAssess(assess);///////////////////////////////////////////////////////////
		
		Label title = new Label("Score Sheet");
		title.setPrimaryStyleName("view-title");
		addComponent(title);
		
		addComponent(buildTitle());
		addComponent(buildCriteria());
		addComponent(buildTable());
		
		Button but = new Button("Save Scores", e -> {
			ArrayList<Mark> mark = new ArrayList<>();
			// Iterate over the item identifiers of the table.
			for (Iterator i = grid.getContainerDataSource().getItemIds().iterator(); i.hasNext();) {
			    // Get the current item identifier, which is an integer.
			    Mark m = (Mark) i.next();
			    
			    mark.add(m);
			    // Now get the actual item from the table.
//			    Item item = grid.getContainerDataSource().getItem(iid);

			}
			try {
				pushScore(mark);
			} catch (JSONException e1) {
				e1.printStackTrace();
			}
			
        });
		but.addStyleName(ValoTheme.BUTTON_FRIENDLY);
		addComponent(but);
		
		try {
			popCombo();
		} catch (JSONException e1) {
			e1.printStackTrace();
		}
	}
	
	private void pushScore(ArrayList<Mark> mark) throws JSONException{
		c = new Controller();
				
		JSONObject json = new JSONObject();
		json.put("tenantID", VaadinSession.getCurrent().getAttribute("tenantID"));
		json.put("term", term.getValue());
		json.put("assess", assess.getValue());
		json.put("session", session.getValue());
		json.put("subject", subject.getValue());
		json.put("klass", klass.getValue());
		
		JSONArray array = new JSONArray();
		for (int i=0; i < mark.size(); i++) {
			JSONObject y = new JSONObject();
	        y.put("regno", mark.get(i).getRegno());
	        y.put("name", mark.get(i).getName());
	        y.put("score", mark.get(i).getScore());
	        array.put(y);
		}
		json.put("mark", array);
		System.out.println(json);
		
		if(isEdit){
			String h = c.postRequest("admin/resultu", json);
			JSONObject oj = new JSONObject(h);
			if(oj.getInt("response") == 1){
				Notify.notify("Scores have been Updated");
			}else{
				Notify.notify("Scores Failed to Update");
			}
		}else{
			String h = c.postRequest("admin/result", json);
			JSONObject oj = new JSONObject(h);
			JSONObject j = oj.getJSONObject("response");
			if(j != null){
				Notify.notify("Scores have been Saved");
			}else{
				Notify.notify("Scores are not Saved");
			}
		}
		
	}
	
	private Component buildTitle(){
		HorizontalLayout lay = new HorizontalLayout();
		lay.setSizeFull();
		assess = new Label(user.getAssess().getAssess());
		assess.addStyleName("view-title");
		session = new Label(user.getAssess().getSession());
		session.addStyleName("view-title");
		term = new Label(user.getAssess().getTerm());
		term.addStyleName("view-title");
		
		lay.addComponent(session);
		lay.addComponent(term);
		lay.addComponent(assess);
		
		return lay;
	}

	@SuppressWarnings("deprecation")
	private Component buildCriteria(){
		
		HorizontalLayout lay = new HorizontalLayout();
		lay.setSizeFull();
		
		subject = new ComboBox("Select Subject");
		subject.setWidth("40%");
		lay.addComponent(subject);
		
		klass = new ComboBox("Select Class");
		klass.setWidth("40%");
		lay.addComponent(klass);
		
		Property.ValueChangeListener listener = new Property.ValueChangeListener() {
		    public void valueChange(ValueChangeEvent event) {
		        try {
					loadStudent(klass.getValue().toString());
				} catch (JSONException e) {
					e.printStackTrace();
				}
		    }
		};
		klass.addListener(listener);
		
		return lay;
	}
	
	private Component buildTable(){
		
		container = new BeanItemContainer<Mark>(Mark.class);
		
		// Create a grid
		grid = new Grid();
	
		grid.addColumn("regno");
		grid.addColumn("name");
		grid.addColumn("score");
		
		Grid.Column a = grid.getColumn("regno");
		a.setHeaderCaption("Registeration No.");		
		Grid.Column b = grid.getColumn("name");
		b.setHeaderCaption("Student Name");
		
		
		grid.setWidth("1000px");
		grid.setResponsive(true);
		grid.setEditorEnabled(true);
		
		grid.addSelectionListener(selectionEvent -> { 
			
			// Create a context menu for 'someComponent'
			ContextMenu contextMenu = new ContextMenu(grid, true);
			contextMenu.setAsContextMenuOf(grid);
			
			Mark selected = (Mark)((SingleSelectionModel)grid.getSelectionModel()).getSelectedRow();
			if (selected != null){

				Notify.notify(selected.toString());
				
			}
			else{
				Notify.notify("Nothing selected");
			}
		});
		
		return grid;
	}

	private void popCombo() throws JSONException{
		c = new Controller();
		JSONObject o = new JSONObject();
		o.put("tenantID", VaadinSession.getCurrent().getAttribute("tenantID"));
		String response = c.postRequest("admin/subjects", o);
		String response2 = c.postRequest("admin/classes", o);
		
		if(response != null && response2 != null){
			
			JSONObject jp = new JSONObject(response);
			JSONArray aj = jp.getJSONArray("response");
			
			JSONObject ab = new JSONObject(response2);
			JSONArray b = ab.getJSONArray("response");
			
			for(int i = 0; i < b.length(); i++){
				JSONObject k = b.getJSONObject(i);
				klass.addItem(k.getString("klassName"));
				
			}
			for(int j = 0; j < aj.length(); j++ ){
				JSONObject d = aj.getJSONObject(j);
				subject.addItem(d.getString("subjectName"));
			}
				
		}
	}
	
	private void loadStudent(String klas) throws JSONException{
		c = new Controller();
		container.removeAllItems();
		
		JSONObject o = new JSONObject();
		o.put("tenantID", VaadinSession.getCurrent().getAttribute("tenantID"));
		String response = c.postRequest("admin/allstudent", o);
		
		
		o.put("session", session.getValue());
		o.put("term", term.getValue());
		o.put("klass", klass.getValue());
		o.put("subject", subject.getValue());
		o.put("assess", assess.getValue());
		String response2 = c.postRequest("admin/results", o);
		
		if(response != null){
			
			JSONObject jp = new JSONObject(response);
			JSONArray aj = jp.getJSONArray("response");
			
			JSONObject d = new JSONObject(response2);
			System.out.println("This is d : "+ d);
			
			if(d.getString("response") == "null"){
				setEdit(false);
				for(int i = 0; i < aj.length(); i++){
					user = new Mark();
					JSONObject j = aj.getJSONObject(i);
					if(klas.equals(j.getString("klass"))){
						user.setRegno(j.getString("regno"));
						String a = j.getString("surname");
						String b = j.getString("middle");
						String c = j.getString("other");
						user.setName(a + " " + b + " " + c);
						user.setScore("0");
						container.addBean(user);
					}
				}
						
				grid.setContainerDataSource(container);
				grid.markAsDirty();
			}
			else{
				setEdit(true);
				JSONObject f = d.getJSONObject("response");
				JSONArray e = f.getJSONArray("mark");
				System.out.println(" Na me : " + e);
				for(int i = 0; i < e.length(); i++){
					user = new Mark();
					JSONObject j = e.getJSONObject(i);
					
						user.setRegno(j.getString("regno"));
						user.setName(j.getString("name"));
						user.setScore(j.getString("score"));
						container.addBean(user);
						
				}
							
				grid.setContainerDataSource(container);
				grid.markAsDirty();
			}
		}
	}
	
	private void loadAssess() throws JSONException{
		c = new Controller();
		container.removeAllItems();
		
		JSONObject o = new JSONObject();
		o.put("tenantID", VaadinSession.getCurrent().getAttribute("tenantID"));
		String response = c.postRequest("admin/assesses", o);
		
		if(response != null){
			
			JSONObject jp = new JSONObject(response);
			JSONArray aj = jp.getJSONArray("response");
			
			for(int i = 0; i < aj.length(); i++){
				ass = new Assess();
				JSONObject j = aj.getJSONObject(i);
//				user.getAssessID().setAssessID(j.getString("assessID"));
				user.getAssess().setAssess(j.getString("assess"));
				
				container.addBean(user);
			}
			System.out.println("Assess : " + user);		
			grid.setContainerDataSource(container);
		}
		
	}
}
