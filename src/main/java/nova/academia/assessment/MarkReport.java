package nova.academia.assessment;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.vaadin.ui.VerticalLayout;

import nova.academia.util.Controller;
import nova.academia.util.Notify;

public class MarkReport extends VerticalLayout{

	public MarkReport(){
		
		try {
			show();
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}
	
	private void show() throws JSONException{
		Controller c = new Controller();
		String h = c.getResponse("admin/allresult");
		JSONObject oj = new JSONObject(h);
		JSONArray j = oj.getJSONArray("response");
				
		JSONArray m = j.getJSONArray(0);
		if(h != null){
			Notify.notify(m.toString());
		}else{
			Notify.notify("Score not Saved");
		}
	}
}
