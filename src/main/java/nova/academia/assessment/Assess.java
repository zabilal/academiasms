package nova.academia.assessment;

import java.util.ArrayList;

public class Assess {
	
	private String assess;
	private String session;
	private String term;
	private String tenantID;
	private String assessID;
	private String status;
	private ArrayList mama;
	
	public String getAssess() {
		return assess;
	}
	public void setAssess(String assess) {
		this.assess = assess;
	}
	public String getSession() {
		return session;
	}
	public void setSession(String session) {
		this.session = session;
	}
	public String getTerm() {
		return term;
	}
	public void setTerm(String term) {
		this.term = term;
	}
	public String getTenantID() {
		return tenantID;
	}
	public void setTenantID(String tenantID) {
		this.tenantID = tenantID;
	}
	public String getAssessID() {
		return assessID;
	}
	public void setAssessID(String assessID) {
		this.assessID = assessID;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	public ArrayList getMama() {
		return mama;
	}
	public void setMama(ArrayList mama) {
		this.mama = mama;
	}
	@Override
	public String toString() {
		return "Assess [assess=" + assess + ", session=" + session + ", term=" + term + ", tenantID=" + tenantID
				+ ", assessID=" + assessID + ", status=" + status + "]";
	}
	
	
	
}
