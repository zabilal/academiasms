package nova.academia.assessment;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.vaadin.addon.contextmenu.ContextMenu;
import com.vaadin.addon.contextmenu.MenuItem;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.server.Responsive;
import com.vaadin.server.VaadinSession;
import com.vaadin.ui.Button;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.Component;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.Grid;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Notification;
import com.vaadin.ui.PasswordField;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import com.vaadin.ui.Grid.SingleSelectionModel;
import com.vaadin.ui.Notification.Type;
import com.vaadin.ui.themes.ValoTheme;

import nova.academia.settings.view.Register;
import nova.academia.util.Controller;
import nova.academia.util.Notify;
import nova.academia.util.State;
import nova.academia.util.Storage;

public class AssessView extends HorizontalLayout{

	private static boolean isEdit;
	private Controller c;
	private Assess user;
	private BeanItemContainer<Assess> container;
	

	private TextField name;
	private ComboBox session;
	private ComboBox term;
	private ComboBox status;
	private Grid grid;
	
	public static boolean isEdit() {
		return isEdit;
	}

	public static void setEdit(boolean isEdit) {
		AssessView.isEdit = isEdit;
	}

	
	public AssessView(){
		
		Responsive.makeResponsive(this);
		
		setWidth("100%");
		setSizeFull();
		setMargin(true);
		setSpacing(true);
		//setComponentAlignment(form, Alignment.MIDDLE_CENTER);
		addStyleName("users-view");
		addStyleName("material");
			
		addComponent(buildUser());
		addComponent(buildTable());
		
		try {
			popCombo();
			onLoad();
//			popCombo();
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
	}
	
	private FormLayout buildUser(){
		
		FormLayout form = new FormLayout();
		form.setSizeFull();
		form.setCaption("Create Assessment");
		
		name = new TextField("Name");
		name.setWidth("80%");
		form.addComponent(name);
		
		session = new ComboBox("Session");
		session.setWidth("80%");
		form.addComponent(session);
		
		term = new ComboBox("Term");
		term.setWidth("80%");
		form.addComponent(term);
		
		status = new ComboBox("Status");
		status.setWidth("80%");
		status.addItems("Open", "Closed");
		form.addComponent(status);
				
		HorizontalLayout hb = new HorizontalLayout();
		hb.setSpacing(true);
		hb.setMargin(true);
		hb.setWidth("80%");
		
		Button save = new Button("Add Assessment");
		save.addClickListener(event -> {
			try {
				fireSaveEvent();
			} catch (JSONException e) {
				e.printStackTrace();
			}
		});
		save.addStyleName(ValoTheme.BUTTON_FRIENDLY);
		
				
		hb.addComponent(save);
		
		form.addComponent(hb);
		
				
		return form;
		
		
	}
	
	private Component buildTable(){
	
		container = new BeanItemContainer<Assess>(Assess.class);
		
		// Create a grid
		grid = new Grid("Assessments");
	
		grid.addColumn("assess");
		grid.addColumn("session");
		grid.addColumn("term");
		grid.addColumn("status");
		
				
		Grid.Column b = grid.getColumn("assess");
		b.setHeaderCaption("Assessment");
		
		
		grid.setSizeFull();
		grid.setResponsive(true);
		grid.setEditorEnabled(true);
		
		grid.addSelectionListener(selectionEvent -> { 
			
			// Create a context menu for 'someComponent'
			ContextMenu contextMenu = new ContextMenu(grid, true);
			contextMenu.setAsContextMenuOf(grid);
			
			Assess selected = (Assess)((SingleSelectionModel)grid.getSelectionModel()).getSelectedRow();
			if (selected != null){
				term.setValue(selected.getTerm());
				session.setValue(selected.getSession());
				name.setValue(selected.getAssess());
				status.setValue(selected.getStatus());
				
				Storage.setUserid(selected.getAssessID());
				setEdit(true);
				
				final MenuItem item = contextMenu.addItem("Mark", e -> {
					if(selected.getStatus().equalsIgnoreCase("Open")){
						Window win = new Window();
						win.setContent(new MarkerView(selected));
						win.setModal(true);
						win.setClosable(true);
						this.getUI().addWindow(win);
					}else{
						Notify.notify("This Assessment is Closed, Contact your Administrator");
					}
					
		        });
				final MenuItem item3 = contextMenu.addItem("View Report", e -> {
		            Notification.show(selected.toString());
		        });
				contextMenu.addSeparator();
				final MenuItem item5 = contextMenu.addItem("Delete Assessment", e -> {
		            Notification.show(selected.toString());
		        });
			}
			else{
				Notification.show("Nothing selected");
			}
		});
		
		return grid;
	}
	
	private void fireSaveEvent() throws JSONException {
		c = new Controller();
		
		JSONObject json = new JSONObject();
		json.put("tenantID", VaadinSession.getCurrent().getAttribute("tenantID"));
		json.put("term", term.getValue());
		json.put("assess", name.getValue());
		json.put("session", session.getValue());
		json.put("term", term.getValue());
		json.put("status", status.getValue());

		
		if(!isEdit){
			System.out.println("Saving Object");
			String h = c.postRequest("admin/assess", json);
			JSONObject oj = new JSONObject(h);
			JSONObject j = oj.getJSONObject("response");
			if(j != null){
				Notification.show("Assessment Created", Type.HUMANIZED_MESSAGE);
				clear();
				onLoad();
			}
		}else{
			json.put("assessID", Storage.getUserid());
			String h = c.postRequest("admin/assessus", json);
			JSONObject j = new JSONObject(h);
			if(j.getInt("response") == 1){
				Notify.notify("Assessment Updated");
				onLoad();
				clear();
//				setEdit(false);
			}else{
				Notify.notify("Assessment Failed to Update");
			}
		}
	}
	
	private void onLoad() throws JSONException{
		c = new Controller();
		container.removeAllItems();
		
		JSONObject o = new JSONObject();
		o.put("tenantID", VaadinSession.getCurrent().getAttribute("tenantID"));
		String response = c.postRequest("admin/assesses", o);
		
		if(response != null){
			
			JSONObject jp = new JSONObject(response);
			JSONArray aj = jp.getJSONArray("response");
			
			for(int i = 0; i < aj.length(); i++){
				user = new Assess();
				JSONObject j = aj.getJSONObject(i);
				user.setAssessID(j.getString("assessID"));
				user.setTerm(j.getString("term"));
				user.setSession(j.getString("session"));
				user.setAssess(j.getString("assess"));
				user.setStatus(j.getString("status"));
				user.setTenantID(j.getString("tenantID"));
				
				container.addBean(user);
			}
						
			grid.setContainerDataSource(container);
		}
		
	}
	
	private void popCombo() throws JSONException{
		c = new Controller();
		JSONObject o = new JSONObject();
		o.put("tenantID", VaadinSession.getCurrent().getAttribute("tenantID"));
		String response = c.postRequest("admin/sessions", o);
		String response2 = c.postRequest("admin/terms", o);
		
		if(response != null && response2 != null){
			
			JSONObject jp = new JSONObject(response);
			JSONArray aj = jp.getJSONArray("response");
			
			JSONObject ab = new JSONObject(response2);
			JSONArray b = ab.getJSONArray("response");
			
			for(int i = 0; i < aj.length(); i++){
			
				JSONObject j = aj.getJSONObject(i);
				if(j.getString("status").equals("OPEN")){
					session.addItem(j.getString("session"));
				}
								
			}
			
			for(int i = 0; i < b.length(); i++){
				
				JSONObject k = b.getJSONObject(i);
				if(k.getString("status").equals("Open")){
					term.addItem(k.getString("term"));
				}
								
			}
				
		}
	}
	
	private void clear(){
		setEdit(false);
		term.setValue(null);
		name.setValue("");
		session.setValue(null);
		status.setValue(null);
	}
	
}

