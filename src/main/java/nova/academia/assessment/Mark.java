package nova.academia.assessment;

public class Mark {
	
	private Assess assess;
	private String regno;
	private String name;
	private String score;
	
	public Assess getAssess() {
		return assess;
	}
	public void setAssess(Assess assess) {
		this.assess = assess;
	}
	public String getRegno() {
		return regno;
	}
	public void setRegno(String regno) {
		this.regno = regno;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getScore() {
		return score;
	}
	public void setScore(String score) {
		this.score = score;
	}
	@Override
	public String toString() {
		return "Mark [assess=" + assess + ", regno=" + regno + ", name=" + name + ", score=" + score + "]";
	}
	
	

}
