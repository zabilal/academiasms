package nova.academia.settings.view;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.data.util.filter.SimpleStringFilter;
import com.vaadin.server.Responsive;
import com.vaadin.server.VaadinSession;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.Component;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.Grid;
import com.vaadin.ui.Grid.HeaderCell;
import com.vaadin.ui.Grid.HeaderRow;
import com.vaadin.ui.Grid.SingleSelectionModel;
import com.vaadin.ui.Notification.Type;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Notification;
import com.vaadin.ui.PasswordField;
import com.vaadin.ui.Table;
import com.vaadin.ui.TextField;
import com.vaadin.ui.Upload;
import com.vaadin.ui.themes.ValoTheme;

import nova.academia.settings.entity.User;
import nova.academia.util.Controller;
import nova.academia.util.State;
import nova.academia.util.Storage;

public class Users extends HorizontalLayout{

	private byte [] pic;
	private static boolean isEdit;
	private Controller c;
	private User user;
	private BeanItemContainer<User> container;
	
	private ComboBox role;
	private TextField fullname;
	private TextField phone;
	private TextField email;
	private TextField username;
	private PasswordField password;
	private Grid grid;
	
	public static boolean isEdit() {
		return isEdit;
	}

	public static void setEdit(boolean isEdit) {
		Users.isEdit = isEdit;
	}

	
	public Users(){
		
		Responsive.makeResponsive(this);
		
		setWidth("100%");
		setSizeFull();
		setMargin(true);
		setSpacing(true);
		//setComponentAlignment(form, Alignment.MIDDLE_CENTER);
		addStyleName("users-view");
		addStyleName("material");
		
		
		
		addComponent(buildUser());
		addComponent(buildTable());
		
		try {
			onLoad();
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}
	
	private FormLayout buildUser(){
		
		FormLayout form = new FormLayout();
		form.setSizeFull();
		form.setCaption("Add Users");
		
		role = new ComboBox("User Role");
		role.setWidth("80%");
		role.addItems(State.ADMIN, State.ACCOUNTANT, State.FRONTDESK, State.LIBRARIAN, State.PARENT, State.STOREKEEPER, State.STUDENT, State.TEACHER);
		form.addComponent(role);
		
		fullname = new TextField("Full Name");
		fullname.setWidth("80%");
		form.addComponent(fullname);
		
		phone = new TextField("Mobile Phone");
		phone.setWidth("80%");
		form.addComponent(phone);
		
		email = new TextField("Email");
		email.setWidth("80%");
		form.addComponent(email);
		
		username = new TextField("Username");
		username.setWidth("80%");
		form.addComponent(username);
		
		password = new PasswordField("Password");
		password.setWidth("80%");
		form.addComponent(password);
		
		HorizontalLayout hb = new HorizontalLayout();
		hb.setSpacing(true);
		hb.setMargin(true);
		hb.setWidth("80%");
		
		Button save = new Button("Save User");
		save.addClickListener(event -> {
			try {
				fireSaveEvent();
			} catch (JSONException e) {
				e.printStackTrace();
			}
		});
		save.addStyleName(ValoTheme.BUTTON_FRIENDLY);
		
		Button edit = new Button("New User");
		edit.addClickListener(event -> {
			clear();
		});
		edit.addStyleName(ValoTheme.BUTTON_DANGER);
		
		hb.addComponents(save, edit);
		
		form.addComponent(hb);
		
				
		return form;
		
		
	}
	
	private Component buildTable(){
	
		container = new BeanItemContainer<User>(User.class);
		
		// Create a grid
		grid = new Grid("User Details");
//		
		grid.addColumn("role");
		grid.addColumn("fullname");
		grid.addColumn("phone");
		grid.addColumn("email");
		grid.addColumn("username");
		grid.setSizeFull();
		grid.setResponsive(true);
		
		grid.addSelectionListener(selectionEvent -> { 
			
			User selected = (User)((SingleSelectionModel)grid.getSelectionModel()).getSelectedRow();
			if (selected != null){
				System.out.println(Storage.getId());
				fullname.setValue(selected.getFullname());
				role.setValue(selected.getRole());
				phone.setValue(selected.getPhone());
				email.setValue(selected.getEmail());
				username.setValue(selected.getUsername());
				password.setValue(selected.getPassword());
				Storage.setUserid(selected.getUserID());
				setEdit(true);
			}
			else{
				Notification.show("Nothing selected");
			}
		});
		
		return grid;
	}
	
	private void fireSaveEvent() throws JSONException {
		c = new Controller();
		
		JSONObject json = new JSONObject();
		json.put("tenantID", VaadinSession.getCurrent().getAttribute("tenantID"));
		json.put("role", role.getValue());
		json.put("fullname", fullname.getValue());
		json.put("phone", phone.getValue());
		json.put("email", email.getValue());
		json.put("username", username.getValue());
		json.put("password", password.getValue());
		
		if(!isEdit){
			
			String h = c.postRequest("admin/inter2", json);
			JSONObject oj = new JSONObject(h);
			JSONObject j = oj.getJSONObject("response");
			if(j != null){
				Notification.show("User Info Saved", Type.HUMANIZED_MESSAGE);
				clear();
				onLoad();
			}
		}else{
			json.put("userID", Storage.getUserid());
			String h = c.postRequest("admin/interu", json);
			JSONObject j = new JSONObject(h);
			if(j.getInt("response") == 1){
				Notification.show("User Info Updated", Type.HUMANIZED_MESSAGE);
				onLoad();
			}else{
				Notification.show("User Info Failed to Update", Type.HUMANIZED_MESSAGE);
			}
		}
	}
	
	private void onLoad() throws JSONException{
		c = new Controller();
		container.removeAllItems();
		
		JSONObject o = new JSONObject();
		o.put("tenantID", VaadinSession.getCurrent().getAttribute("tenantID"));
		String response = c.postRequest("admin/intera", o);
		
		if(response != null){
			
			JSONObject jp = new JSONObject(response);
			JSONArray aj = jp.getJSONArray("response");
			
			for(int i = 0; i < aj.length(); i++){
				user = new User();
				JSONObject j = aj.getJSONObject(i);
				user.setId(j.getLong("id"));
				user.setUserID(j.getString("userID"));
				user.setEmail(j.getString("email"));
				user.setFullname(j.getString("fullname"));
				user.setPassword(j.getString("password"));
				user.setPhone(j.getString("phone"));
				user.setRole(j.getString("role"));
				user.setTenantID(j.getString("tenantID"));
				user.setUsername(j.getString("username"));
				
				container.addBean(user);
			}
						
			grid.setContainerDataSource(container);
		}
		
	}
	
	private void clear(){
		setEdit(false);
		fullname.setValue("");
		role.setValue("");
		phone.setValue("");
		email.setValue("");
		username.setValue("");
		password.setValue("");
	}
	
	private void enableSetup(){
		setEdit(true);
		
		fullname.setEnabled(true);
		role.setEnabled(true);
		phone.setEnabled(true);
		email.setEnabled(true);
		username.setEnabled(true);
		password.setEnabled(true);
	}
}
