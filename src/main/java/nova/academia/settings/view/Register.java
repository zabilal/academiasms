package nova.academia.settings.view;

import org.json.JSONException;
import org.json.JSONObject;

import com.vaadin.ui.Button;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Notification;
import com.vaadin.ui.PasswordField;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Notification.Type;
import com.vaadin.ui.themes.ValoTheme;

import nova.academia.settings.entity.User;
import nova.academia.util.Controller;
import nova.academia.util.Notify;
import nova.academia.util.State;
import nova.academia.util.Storage;

public class Register extends VerticalLayout{

	private Controller c;
	private User user;
	private ComboBox role;
	private TextField fullname;
	private TextField phone;
	private TextField email;
	private TextField username;
	private PasswordField password;
	
	public Register(){
		setMargin(true);
		addComponent(buildUser());
	}
	
private FormLayout buildUser(){
		
		FormLayout form = new FormLayout();
		form.setSizeFull();
		form.setCaption("Create Admin Account");
		
		role = new ComboBox("User Role");
		role.setWidth("80%");
		role.addItems(State.ADMIN);//, State.ACCOUNTANT, State.FRONTDESK, State.LIBRARIAN, State.PARENT, State.STOREKEEPER, State.STUDENT, State.TEACHER);
		form.addComponent(role);
		
		fullname = new TextField("Full Name");
		fullname.setWidth("80%");
		form.addComponent(fullname);
		
		phone = new TextField("Mobile Phone");
		phone.setWidth("80%");
		form.addComponent(phone);
		
		email = new TextField("Email");
		email.setWidth("80%");
		form.addComponent(email);
		
		username = new TextField("Username");
		username.setWidth("80%");
		form.addComponent(username);
		
		password = new PasswordField("Password");
		password.setWidth("80%");
		form.addComponent(password);
		
		HorizontalLayout hb = new HorizontalLayout();
		hb.setSpacing(true);
		hb.setMargin(true);
		hb.setWidth("80%");
		
		Button save = new Button("Save User");
		save.addClickListener(event -> {
			try {
				fireSaveEvent();
			} catch (JSONException e) {
				e.printStackTrace();
			}
		});
		save.addStyleName(ValoTheme.BUTTON_FRIENDLY);
		
		hb.addComponents(save);
		
		form.addComponent(hb);
		
				
		return form;
		
		
	}

	private void fireSaveEvent() throws JSONException {
		c = new Controller();
		
		JSONObject json = new JSONObject();
		json.put("tenantID", Storage.getTenantID());
		json.put("role", role.getValue());
		json.put("fullname", fullname.getValue());
		json.put("phone", phone.getValue());
		json.put("email", email.getValue());
		json.put("username", username.getValue());
		json.put("password", password.getValue());
	
	
		
		String h = c.postRequest("admin/inter", json);
		JSONObject oj = new JSONObject(h);
		JSONObject j = oj.getJSONObject("response");
		if(j != null){
			Notify.notify("User Info Saved");
		}else{
			Notify.notify("User Info not Saved");
		}
	
	}
}
