package nova.academia.settings.view;

import com.vaadin.event.ContextClickEvent.ContextClickListener;
import com.vaadin.server.Responsive;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Component;
import com.vaadin.ui.Grid;
import com.vaadin.ui.Label;
import com.vaadin.ui.VerticalLayout;

public class Gateway extends VerticalLayout{

	public Gateway(){
		
		Responsive.makeResponsive(this);
		
		Label title = new Label("Payment Gateway");
		title.setPrimaryStyleName("label-title");
		addComponent(title);
		
		// Create a grid
		Grid grid = new Grid();
		
		// Define some columns
		grid.addColumn("S/N", Integer.class);
		grid.addColumn("Payment Gateway", String.class);
		grid.addColumn("Gateway Status", String.class);
		grid.addColumn("Manage Gateway", Component.class);
		
		// Add some data rows
		grid.addRow(1, "Flutterwave", "Active", new Button("Manage"));
		grid.addRow(2, "GTPay (MasterCard/Visa/Verve)", "Active", new Button("Manage"));
		grid.addRow(3, "GTBank Transfer", "Active", new Button("Manage"));
		grid.addRow(4, "Interswitch (MasterCard/Visa/Verve)", "Active", new Button("Manage"));
		grid.addRow(5, "Pay by Cash", "Active", new Button("Manage"));
		
		grid.setSizeFull();
		grid.setResponsive(true);
	
		
		setWidth("80%");
		setMargin(true);
		setSpacing(true);
//		setDefaultComponentAlignment(Alignment.MIDDLE_CENTER);
		addStyleName("gate-view");
		
		addComponent(grid);
	}
}
