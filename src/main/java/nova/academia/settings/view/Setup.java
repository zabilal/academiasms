package nova.academia.settings.view;

import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.imageio.ImageIO;

import org.apache.commons.codec.binary.Base64;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.vaadin.easyuploads.UploadField;
import org.vaadin.easyuploads.UploadField.FieldType;

import com.vaadin.server.Responsive;
import com.vaadin.server.StreamResource;
import com.vaadin.server.VaadinSession;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Image;
import com.vaadin.ui.Label;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Notification.Type;
import com.vaadin.ui.TextField;
import com.vaadin.ui.Upload;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.ValoTheme;

import nova.academia.util.Controller;
import nova.academia.util.Notify;
import nova.academia.util.Storage;


public class Setup  extends VerticalLayout{

	private byte [] logo;
//	private TextField id;
	private TextField name;
	private UploadField up;
	private TextField address;
	private TextField city;
	private TextField lga;
	private TextField state;
	private TextField country;
	private TextField phone;
	private TextField email;
	private TextField site;
	private TextField session;
	private TextField term;
	private Image image;
	
	private Controller c;
	private static boolean isEdit;
		
	public static boolean isEdit() {
		return isEdit;
	}

	public static void setEdit(boolean isEdit) {
		Setup.isEdit = isEdit;
	}

	public Setup(){
		
		Responsive.makeResponsive(this);
		
		Label title = new Label("General Settings");
		title.setPrimaryStyleName("view-title");
		addComponent(title);
		
		
		FormLayout form = new FormLayout();
		form.setMargin(true);
		form.setSpacing(true);
		
		name = new TextField("School Name");
		name.setWidth("50%");
		form.addComponent(name);
		
		up = new UploadField();
		up.setButtonCaption("Select Logo...");
		up.setWidth("50%");
		up.setCaption("School Logo");
		up.setAcceptFilter("image/*");
		up.setFieldType(FieldType.BYTE_ARRAY);
		
		image = new Image("Logo Preview");
		up.addListener(new Listener(){
	        @Override
	        public void componentEvent(Event event)
	        {
	        	Object value = up.getValue();
	            logo = (byte[]) value;
	            showUploadedImage(up, image);
	        }
	    });
		
		form.addComponent(up);
//		form.addComponent(image);
		
		address = new TextField("School Address");
		address.setWidth("50%");
		form.addComponent(address);
		
		city = new TextField("Town/City");
		city.setWidth("50%");
		form.addComponent(city);
		
		lga = new TextField("Local Council");
		lga.setWidth("50%");
		form.addComponent(lga);
		
		state = new TextField("State/Region");
		state.setWidth("50%");
		form.addComponent(state);
		
		country = new TextField("Country");
		country.setWidth("50%");
		form.addComponent(country);
		
		phone = new TextField("Phone Number");
		phone.setWidth("50%");
		form.addComponent(phone);
		
		email = new TextField("Email Address");
		email.setWidth("50%");
		form.addComponent(email);
		
		site = new TextField("School Website");
		site.setWidth("50%");
		form.addComponent(site);
		
		session = new TextField("Current Session");
		session.setWidth("50%");
		form.addComponent(session);
		
		term = new TextField("Curent Term");
		term.setWidth("50%");
		form.addComponent(term);
		
		
		HorizontalLayout hb = new HorizontalLayout();
		hb.setSpacing(true);
		hb.setMargin(true);
		hb.setWidth("50%");
		
		Button save = new Button("Save Setup");
		save.addClickListener(event -> {
			try {
				fireSaveEvent();
			} catch (JSONException e) {
				e.printStackTrace();
			}
		});
		save.addStyleName(ValoTheme.BUTTON_FRIENDLY);
		
		Button edit = new Button("Edit Setup");
		edit.addClickListener(event -> {
			enableSetup();
		});
		edit.addStyleName(ValoTheme.BUTTON_DANGER);
		
		hb.addComponents(save, edit);
		
		form.addComponent(hb);
		
		setWidth("80%");
		setMargin(true);
		setSpacing(true);
		

		addComponent(form);
		setComponentAlignment(form, Alignment.MIDDLE_CENTER);
		addStyleName("setup-view");
		addStyleName("material");
		
		//Calling onLoad to Fill Form from DB
		try {
			onLoad();
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
	}
	
	private void showUploadedImage(UploadField upload, Image image) {
	    Object value = upload.getValue();
	    final byte[] data = (byte[]) value;
	 
	    StreamResource resource = new StreamResource(
	        new StreamResource.StreamSource() {
	            @Override
	            public InputStream getStream() {
	                 try {
	                  BufferedImage bi = ImageIO.read(new ByteArrayInputStream(data));
	                  
	                  int scaleX = (int) (bi.getWidth() * 0.25);
	                  int scaleY = (int) (bi.getHeight() * 0.25);
	                  
	                  java.awt.Image newImg = bi.getScaledInstance(scaleX, scaleY, java.awt.Image.SCALE_SMOOTH);
	                  /* Write the image to a buffer. */
	                  ByteArrayOutputStream imagebuffer = new ByteArrayOutputStream();
	                  BufferedImage bimage = new BufferedImage(scaleX, scaleY, BufferedImage.TYPE_INT_RGB);
	                  Graphics bg = bimage.getGraphics();
	                  bg.drawImage(newImg, 0, 0, null);
	                  bg.dispose();
	                  ImageIO.write(bimage, "jpg", imagebuffer);
	                  
	                  /* Return a stream from the buffer. */
	                  return new ByteArrayInputStream(imagebuffer.toByteArray());
	                } catch (IOException e) {
	                    return null;
	                }
	            }
	        }, "filename.png");
	    
	    image.setSource(resource);
//	    image.setVisible(true);
	}

	private void fireSaveEvent() throws JSONException {
		// TODO Auto-generated method stub
		c = new Controller();
		String logos = Base64.encodeBase64String(logo);
		
		JSONObject json = new JSONObject();
		json.put("tenantID", VaadinSession.getCurrent().getAttribute("tenantID"));
		json.put("schoolName", name.getValue());
		json.put("address", address.getValue());
		json.put("city", city.getValue());
		json.put("lga", lga.getValue());
		json.put("state", state.getValue());
		json.put("country", country.getValue());
		json.put("phone", phone.getValue());
		json.put("email", email.getValue());
		json.put("website", site.getValue());
		json.put("activeSession", session.getValue());
		json.put("activeTerm", term.getValue());
		
		
		if(!isEdit){
			json.put("logo", logos);
			String h = c.postRequest("admin/setting", json);
			JSONObject oj = new JSONObject(h);
			JSONObject j = oj.getJSONObject("response");
			if(j != null){
				Notify.notify("School Settings Saved");
			}else{
				Notify.notify("School Settings not Saved");
			}
		}else{
			String h = c.postRequest("admin/settingsu", json);
			JSONObject j = new JSONObject(h);
			if(j.getInt("response") == 1){
				Notification.show("School Setup Updated", Type.HUMANIZED_MESSAGE);
			}else{
				Notification.show("School Setup Fail to Update", Type.HUMANIZED_MESSAGE);
			}
		}
	}
	
	private void onLoad() throws JSONException{
		c = new Controller();
		JSONObject js = new JSONObject();
		js.put("tenantID", VaadinSession.getCurrent().getAttribute("tenantID"));
		String response = c.postRequest("admin/settingid", js);
		if(response != null){
			
			JSONObject jp = new JSONObject(response);
			JSONObject j = jp.getJSONObject("response");
			
//			id.setValue(j.getString("tenantID"));
			name.setValue(j.getString("schoolName"));
			address.setValue(j.getString("address"));
			city.setValue(j.getString("city"));
			lga.setValue(j.getString("lga"));
			state.setValue(j.getString("state"));
			country.setValue(j.getString("country"));
			phone.setValue(j.getString("phone"));
			email.setValue(j.getString("email"));
			site.setValue(j.getString("website"));
			session.setValue(j.getString("activeSession"));
			term.setValue(j.getString("activeTerm"));
			
			
			
		}
		
		disableSetup();
	}
	
	private void disableSetup(){
//		id.setEnabled(false);
		name.setEnabled(false);
		address.setEnabled(false);
		city.setEnabled(false);
		lga.setEnabled(false);
		state.setEnabled(false);
		country.setEnabled(false);
		phone.setEnabled(false);
		email.setEnabled(false);
		site.setEnabled(false);
		session.setEnabled(false);
		term.setEnabled(false);
	}
	
	private void enableSetup(){
		setEdit(true);
		System.out.println(isEdit);
//		id.setEnabled(true);
		name.setEnabled(true);
		address.setEnabled(true);
		city.setEnabled(true);
		lga.setEnabled(true);
		state.setEnabled(true);
		country.setEnabled(true);
		phone.setEnabled(true);
		email.setEnabled(true);
		site.setEnabled(true);
		session.setEnabled(true);
		term.setEnabled(true);
	}
}
