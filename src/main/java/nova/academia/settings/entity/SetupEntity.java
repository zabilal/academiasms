package nova.academia.settings.entity;


public class SetupEntity {

	private String tenantID;
    private String schoolName, address, city, lga, state, country, phone, email, website;
    private byte [] logo;
    private byte [] background;
//    
//    //Academic Settings
    private String activeSession, activeTerm, gradClass;


    public String getTenantID() {
        return tenantID;
    }

    public void setTenantID(String tenantID) {
        this.tenantID = tenantID;
    }

    public String getSchoolName() {
        return schoolName;
    }

    public void setSchoolName(String SchoolName) {
        this.schoolName = SchoolName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getLga() {
        return lga;
    }

    public void setLga(String lga) {
        this.lga = lga;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public byte[] getLogo() {
        return logo;
    }

    public void setLogo(byte[] logo) {
        this.logo = logo;
    }

    public byte[] getBackground() {
        return background;
    }

    public void setBackground(byte[] background) {
        this.background = background;
    }

    public String getActiveSession() {
        return activeSession;
    }

    public void setActiveSession(String activeSession) {
        this.activeSession = activeSession;
    }

    public String getActiveTerm() {
        return activeTerm;
    }

    public void setActiveTerm(String activeTerm) {
        this.activeTerm = activeTerm;
    }

    public String getGradClass() {
        return gradClass;
    }

    public void setGradClass(String gradClass) {
        this.gradClass = gradClass;
    }

    @Override
    public String toString() {
        return "Settings{" + "tenantID=" + tenantID + ", SchoolName=" + schoolName + ", address=" + address + ", city=" + city + ", lga=" + lga + ", state=" + state + ", country=" + country + ", phone=" + phone + ", email=" + email + ", website=" + website + ", logo=" + logo + ", background=" + background + ", activeSession=" + activeSession + ", activeTerm=" + activeTerm + ", gradClass=" + gradClass + "}";
    }
}
