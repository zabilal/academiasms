package nova.academia.nav;

import com.vaadin.server.FontAwesome;
import com.vaadin.server.Resource;
import com.vaadin.server.Responsive;
import com.vaadin.server.ThemeResource;
import com.vaadin.ui.Accordion;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Layout;
import com.vaadin.ui.Notification;
import com.vaadin.ui.VerticalLayout;

import nova.academia.mainview.MainView;
import nova.academia.settings.control.MenuButtonHandler;

import com.vaadin.ui.TabSheet.SelectedTabChangeEvent;
import com.vaadin.ui.TabSheet.SelectedTabChangeListener;

public class NovaSide extends CssLayout{
	
	private Button b, b2;//Menu Buttons
	private MenuButtonHandler handler; 
	private MainView main;

	public NovaSide(){
		
		Responsive.makeResponsive(this);
		addComponent(buildLoginInformation());
		handler = new MenuButtonHandler();
		main = new MainView();
	}
	
	 private CssLayout buildLoginInformation() {
	        CssLayout loginInformation = new CssLayout();
	        loginInformation.setStyleName("login-information");
	        
	        loginInformation.addComponent(profile());
	        loginInformation.addComponent(menu());
	        
	        return loginInformation;
	    }
	 
	 private CssLayout profile(){
	    	
	    	CssLayout pro = new CssLayout();
	    	VerticalLayout v = new VerticalLayout();
	    	v.setWidth("250px");
	    	
	    	pro.setPrimaryStyleName("profile");
	    	    	
	    	Resource icon = new ThemeResource("favicon.ico");
	    	Label name = new Label("Raji Zakariyya");
	    	name.setSizeUndefined();
	    	Label ic = new Label();
	    	ic.setSizeUndefined();
	    	ic.setIcon(icon);
	    	
	    	v.addComponents(ic, name);
	    	v.setComponentAlignment(ic, Alignment.MIDDLE_CENTER);
	    	v.setComponentAlignment(name, Alignment.MIDDLE_CENTER);
	    	pro.addComponents(v);
	    	    	
	    	return pro;
	    }
	 
	 private Accordion menu(){
     	
     	CssLayout lay = new CssLayout();
     	
     	Accordion accordion = new Accordion();
     	accordion.setPrimaryStyleName("accordion");
     	
     	Layout tab1 = new VerticalLayout();
     	tab1.setPrimaryStyleName("menu-slide-down");
     	
     	b = new Button("Admit New Student");
     	b.setPrimaryStyleName("sub-menu");
     	b2 = new Button("Manage Applicants");
     	b2.setPrimaryStyleName("sub-menu");
     	
     	tab1.addComponents(b, b2);
     	
     	Layout tab2 = new VerticalLayout();
     	tab2.setPrimaryStyleName("menu-slide-down");
     	for(int i = 0; i < 5; i++){
     		Button b = new Button("Menu "+ i+1);
     		b.setPrimaryStyleName("sub-menu");
     		tab2.addComponent(b);
     	}
     	
     	Layout tab3 = new VerticalLayout();
     	tab3.setPrimaryStyleName("menu-slide-down");
     	for(int i = 0; i < 5; i++){
     		Button b = new Button("Menu "+ i+1);
     		b.setPrimaryStyleName("sub-menu");
     		tab3.addComponent(b);
     	}
     	
     	Layout tab4 = new VerticalLayout();
     	tab4.setPrimaryStyleName("menu-slide-down");
     	for(int i = 0; i < 5; i++){
     		Button b = new Button("Menu "+ i+1);
     		b.setPrimaryStyleName("sub-menu");
     		tab4.addComponent(b);
     	}
     	
     	Layout tab5 = new VerticalLayout();
     	tab4.setPrimaryStyleName("menu-slide-down");
     	for(int i = 0; i < 5; i++){
     		Button b = new Button("Menu "+ i+1);
     		b.setPrimaryStyleName("sub-menu");
     		tab5.addComponent(b);
     	}
     	
     	Layout tab6 = new VerticalLayout();
     	tab6.setPrimaryStyleName("menu-slide-down");
     	
     	b = new Button("Admit New Student");
     	b.setPrimaryStyleName("sub-menu");
     	//b = new Button("Manage Applicants");
     	
     	
     	tab6.addComponent(b);
     	
     	Layout tab7 = new VerticalLayout();
     	tab7.setPrimaryStyleName("menu-slide-down");
     	for(int i = 0; i < 5; i++){
     		Button b = new Button("Menu "+ i+1);
     		b.setPrimaryStyleName("sub-menu");
     		tab7.addComponent(b);
     	}
     	
     	Layout tab8 = new VerticalLayout();
     	tab8.setPrimaryStyleName("menu-slide-down");
     	for(int i = 0; i < 5; i++){
     		Button b = new Button("Menu "+ i+1);
     		b.setPrimaryStyleName("sub-menu");
     		tab8.addComponent(b);
     	}
     	
     	Layout tab9 = new VerticalLayout();
     	tab9.setPrimaryStyleName("menu-slide-down");
     	for(int i = 0; i < 5; i++){
     		Button b = new Button("Menu "+ i+1);
     		b.setPrimaryStyleName("sub-menu");
     		tab9.addComponent(b);
     	}
     	
     	Layout tab10 = new VerticalLayout();
     	tab10.setPrimaryStyleName("menu-slide-down");
     	for(int i = 0; i < 5; i++){
     		Button b = new Button("Menu "+ i+1);
     		b.setPrimaryStyleName("sub-menu");
     		tab10.addComponent(b);
     	}
     	
     	Layout tab11 = new VerticalLayout();
     	tab11.setPrimaryStyleName("menu-slide-down");
     	
     	b = new Button("Admit New Student");
     	b.setPrimaryStyleName("sub-menu");
     	//b = new Button("Manage Applicants");
     	
     	
     	tab11.addComponent(b);
     	
     	Layout tab12 = new VerticalLayout();
     	tab12.setPrimaryStyleName("menu-slide-down");
     	for(int i = 0; i < 5; i++){
     		Button b = new Button("Menu "+ i+1);
     		b.setPrimaryStyleName("sub-menu");
     		tab12.addComponent(b);
     	}
     	
     	Layout tab13 = new VerticalLayout();
     	tab13.setPrimaryStyleName("menu-slide-down");
     	
     		Button b = new Button("School Setup");
     		b.setPrimaryStyleName("sub-menu");
     		b.addClickListener(handler);
     		tab13.addComponent(b);
     	
     	
     	
     	
     	accordion.addTab(tab1, "Dashboard", FontAwesome.DASHBOARD);
     	accordion.addTab(tab2, "Admission", FontAwesome.INSTITUTION);
     	accordion.addTab(tab3, "People", FontAwesome.USERS);
     	accordion.addTab(tab4, "Library", FontAwesome.BOOK);
     	accordion.addTab(tab5, "Documents", FontAwesome.FILE);
     	accordion.addTab(tab6, "Academic Planning", FontAwesome.GRADUATION_CAP);
     	accordion.addTab(tab7, "Fees", FontAwesome.MONEY);
     	accordion.addTab(tab8, "Exams & Records", FontAwesome.DATABASE);
     	accordion.addTab(tab9, "Store & Inventory", FontAwesome.SHOPPING_BASKET);
     	accordion.addTab(tab10, "Hostels & Accomodation", FontAwesome.HOME);
     	accordion.addTab(tab11, "Finance", FontAwesome.MONEY);
     	accordion.addTab(tab12, "Human Reource/Payroll", FontAwesome.DOLLAR);
     	accordion.addTab(tab13, "Tools & Settings", FontAwesome.GEARS);
     	
     	accordion.addSelectedTabChangeListener(new SelectedTabChangeListener() {
     	
     		public void selectedTabChange(SelectedTabChangeEvent event) {
		        	Notification.show("You are watching "
		        	+ event.getTabSheet().getSelectedTab());
		        	}
     	});
	        
	        lay.addComponent(accordion);
	        return accordion;
 }
}
