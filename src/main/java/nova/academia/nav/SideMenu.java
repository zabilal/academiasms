package nova.academia.nav;

import com.vaadin.navigator.Navigator;
import com.vaadin.server.FontAwesome;
import com.vaadin.server.Resource;
import com.vaadin.server.Responsive;
import com.vaadin.ui.Accordion;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Component;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Image;
import com.vaadin.ui.Layout;
import com.vaadin.ui.MenuBar;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.MenuBar.Command;
import com.vaadin.ui.MenuBar.MenuItem;
import com.vaadin.ui.themes.ValoTheme;

/**
 * A helper component to make it easy to create menus like the one in the
 * Dashboard demo. The SideMenu should be the content of the UI, and all
 * other components should be handled through it. The UI should be annotated
 * with {@link SideMenuUI} to provide responsiveness.
 * <p>
 * This component has modification to allow it to be used easily with
 * {@link Navigator}. Pass it as a parameter to the constructor like
 * {@code new Navigator(myUI, sideMenu)}.
 * 
 * @author Raji Zakariyya
 */


@SuppressWarnings("serial")
public class SideMenu extends HorizontalLayout {

	/**
	 * A simple lambda compatible handler class for executing code when a menu
	 * entry is clicked.
	 */
	public interface MenuClickHandler {

		/**
		 * This method is called when associated menu entry is clicked.
		 */
		void click();
	}

	/* Class name for hiding the menu when screen is too small */
	private static final String STYLE_VISIBLE = "valo-menu-visible";

	/* Components to handle content and menus */
	private final VerticalLayout contentArea = new VerticalLayout();
	private final HorizontalLayout topBar = new HorizontalLayout();
	private final VerticalLayout content = new VerticalLayout();
	private final CssLayout menuArea = new CssLayout();//CssLayout
	private final VerticalLayout menuItemsLayout = new VerticalLayout();//CssLayout
	private final MenuBar userMenu = new MenuBar();
	private Accordion accord = new Accordion();
	private HorizontalLayout top2 = new HorizontalLayout();//for setting header bar contents

	/* Quick access to user drop down menu */
	private MenuItem userItem;

	/* Caption component for the whole menu */
	private HorizontalLayout logoWrapper;
	private Image menuImage;

	/**
	 * Constructor for creating a SideMenu component. This method sets up all
	 * the components and styles needed for the side menu.
	 */
	public SideMenu() {
		super();

		addStyleName(ValoTheme.UI_WITH_MENU);
		Responsive.makeResponsive(this);
		setSizeFull();

		menuArea.setPrimaryStyleName("valo-menu");
		menuArea.addStyleName("sidebar");
		menuArea.addStyleName(ValoTheme.MENU_PART);
		menuArea.addStyleName("no-vertical-drag-hints");
		menuArea.addStyleName("no-horizontal-drag-hints");
		menuArea.addStyleName("menu-area");
		menuArea.addStyleName("material");
		menuArea.setHeight("100%");
		menuArea.addStyleName("v-scrollable");

		logoWrapper = new HorizontalLayout();
		logoWrapper.addStyleName("valo-menu-title");
		logoWrapper.addStyleName("logo-color");
		menuArea.addComponent(logoWrapper);

		userMenu.addStyleName("user-menu");
		userItem = userMenu.addItem("", null);
		
		accord.setPrimaryStyleName("accordion");
		accord.addStyleName("v-scrollable");
		accord.setHeight("100%");

		HorizontalLayout top = new HorizontalLayout();//For Setting User Profile Menus
		top.addComponent(userMenu);
		topBar.setDefaultComponentAlignment(Alignment.MIDDLE_RIGHT);//MIne
		topBar.addComponent(top2);//Mine
		topBar.addComponent(top);//Mine
		
		topBar.setExpandRatio(top2, 1);
		
		
		//menuArea.addComponent(userMenu);//Original
		menuItemsLayout.setWidth("250px");
		menuItemsLayout.addStyleName("v-scrollable");//Mine
		menuItemsLayout.addComponent(accord);
		menuItemsLayout.setMargin(true);///Mine
		menuItemsLayout.setSpacing(true);//Mine

		Button valoMenuToggleButton = new Button("Menu", new ClickListener() {
			@Override
			public void buttonClick(final ClickEvent event) {
				if (menuArea.getStyleName().contains(STYLE_VISIBLE)) {
					menuArea.removeStyleName(STYLE_VISIBLE);
				} else {
					menuArea.addStyleName(STYLE_VISIBLE);
				}
			}
		});
		valoMenuToggleButton.setIcon(FontAwesome.LIST);
		valoMenuToggleButton.addStyleName("valo-menu-toggle");
		valoMenuToggleButton.addStyleName(ValoTheme.BUTTON_BORDERLESS);
		valoMenuToggleButton.addStyleName(ValoTheme.BUTTON_SMALL);
		menuArea.addComponent(valoMenuToggleButton);

		menuItemsLayout.addStyleName("valo-menuitems");
		menuArea.addComponent(menuItemsLayout);
		
		contentArea.setPrimaryStyleName("valo-content");
		contentArea.addStyleName("v-scrollable");//Disable to make the header not to scroll
//		contentArea.setMargin(true);//Mine
//		contentArea.setDefaultComponentAlignment(Alignment.MIDDLE_CENTER);
		contentArea.setSizeFull();
		
		topBar.setPrimaryStyleName("valo-content");
		topBar.addStyleName("header");
		topBar.addStyleName("material");//For Material Settings
		topBar.setWidth("100%");
		topBar.setHeight("60px");
		topBar.addStyleName("no-vertical-drag-hints");
		topBar.addStyleName("no-horizontal-drag-hints");
		
		top2.setPrimaryStyleName("valo-content");
		top2.setMargin(true);//Mine
		top2.setDefaultComponentAlignment(Alignment.MIDDLE_CENTER);
		top2.addStyleName("no-vertical-drag-hints");
		top2.addStyleName("no-horizontal-drag-hints");
		top2.setSizeFull();
				
		content.setPrimaryStyleName("valo-content");
		content.setMargin(true);//Mine
		content.setDefaultComponentAlignment(Alignment.MIDDLE_CENTER);
		content.addStyleName("no-vertical-drag-hints");
		content.addStyleName("no-horizontal-drag-hints");
		content.setSizeFull();
				
		contentArea.addComponent(topBar);
		contentArea.addComponent(content);
		contentArea.setExpandRatio(content, 1);
		
		super.addComponent(menuArea);
		super.addComponent(contentArea);
		setExpandRatio(contentArea, 1);
	}

	/**
	 * Adds a menu entry. The given handler is called when the user clicks the
	 * entry.
	 * 
	 * @param text
	 *            menu text
	 * @param handler
	 *            menu click handler
	 */
	public void addMenuItem(String text, MenuClickHandler handler) {
		addMenuItem(text, null, handler);
	}

	/**
	 * Adds a menu entry with given icon. The given handler is called when the
	 * user clicks the entry.
	 * 
	 * @param text
	 *            menu text
	 * @param icon
	 *            menu icon
	 * @param handler
	 *            menu click handler
	 */
	public void addMenuItem(String text, Resource icon, final MenuClickHandler handler) {
		Button button = new Button(text, new ClickListener() {

			@Override
			public void buttonClick(ClickEvent event) {
				handler.click();
				menuArea.removeStyleName(STYLE_VISIBLE);
			}
		});
		button.setIcon(icon);
		button.setPrimaryStyleName("valo-menu-item");
		menuItemsLayout.addComponent(button);
	}

	/**
	 * Adds a menu entry to the user drop down menu. The given handler is called
	 * when the user clicks the entry.
	 * 
	 * @param text
	 *            menu text
	 * @param handler
	 *            menu click handler
	 */
	public void addUserMenuItem(String text, MenuClickHandler handler) {
		addUserMenuItem(text, null, handler);
	}

	/**
	 * Adds a menu entry to the user drop down menu with given icon. The given
	 * handler is called when the user clicks the entry.
	 * 
	 * @param text
	 *            menu text
	 * @param icon
	 *            menu icon
	 * @param handler
	 *            menu click handler
	 */
	public void addUserMenuItem(String text, Resource icon, final MenuClickHandler handler) {
		userItem.addItem(text, icon, new Command() {
			@Override
			public void menuSelected(MenuItem selectedItem) {
				handler.click();
			}
		});
	}

	/**
	 * Sets the user name to be displayed in the menu.
	 * 
	 * @param userName
	 *            user name
	 */
	public void setUserName(String userName) {
		userItem.setText(userName);
	}

	/**
	 * Sets the portrait of the user to be displayed in the menu.
	 * 
	 * @param icon
	 *            portrait of the user
	 */
	public void setUserIcon(Resource icon) {
		userItem.setIcon(icon);
	}

	/**
	 * Sets the visibility of the whole user menu. This includes portrait, user
	 * name and the drop down menu.
	 * 
	 * @param visible
	 *            user menu visibility
	 */
	public void setUserMenuVisible(boolean visible) {
		userMenu.setVisible(visible);
	}

	/**
	 * Gets the visibility of the user menu.
	 * 
	 * @return {@code true} if visible; {@code false} if hidden
	 */
	public boolean isUserMenuVisible() {
		return userMenu.isVisible();
	}

	/**
	 * Sets the title text for the menu
	 * 
	 * @param caption
	 *            menu title
	 */
	public void setMenuCaption(String caption) {
		setMenuCaption(caption, null);
	}

	/**
	 * Sets the title caption and logo for the menu
	 * 
	 * @param caption
	 *            menu caption
	 * @param logo
	 *            menu logo
	 */
	public void setMenuCaption(String caption, Resource logo) {
		if (menuImage != null) {
			logoWrapper.removeComponent(menuImage);
		}
		menuImage = new Image(caption, logo);
		menuImage.setWidth("100%");
		logoWrapper.addComponent(menuImage);
	}

	/**
	 * Removes all content from the user drop down menu.
	 */
	public void clearUserMenu() {
		userItem.removeChildren();
	}

	/**
	 * Adds a menu entry to navigate to given navigation state.
	 * 
	 * @param text
	 *            text to display in menu
	 * @param navigationState
	 *            state to navigate to
	 */
	public void addNavigation(String text, String navigationState) {
		addNavigation(text, null, navigationState);
	}

	/**
	 * Adds a menu entry with given icon to navigate to given navigation state.
	 * 
	 * @param text
	 *            text to display in menu
	 * @param icon
	 *            icon to display in menu
	 * @param navigationState
	 *            state to navigate to
	 */
	public void addNavigation(String text, Resource icon, final String navigationState) {
		addMenuItem(text, icon, new MenuClickHandler() {

			@Override
			public void click() {
				getUI().getNavigator().navigateTo(navigationState);
			}
		});
	}

	/**
	 * Removes all components from the content area.
	 */
	@Override
	public void removeAllComponents() {
		contentArea.removeAllComponents();
	}

	/**
	 * Adds a component to the content area.
	 */
	@Override
	public void addComponent(Component c) {
		contentArea.addComponent(c);
	}

	/**
	 * Removes all content from the content area and replaces everything with
	 * given component.
	 * 
	 * @param content
	 *            new content to display
	 */
	public void setContent(Component c) {
		content.removeAllComponents();
		content.addComponent(c);
	}
	
	/**
	 * Removes all content from the Top Bar area and replaces everything with
	 * given component.
	 * 
	 * @param content
	 *            new content to display
	 */
	public void setBar(Component content) {
		//topBar.removeAllComponents();
		topBar.addComponent(content);
//		topBar.setComponentAlignment(content, alignment);
	}
	
	/**
	 * Removes all content from the Top Bar area and replaces everything with
	 * given component.
	 * 
	 * @param content
	 *            new content to display
	 */
	public void setHeaderContent(Component content) {
		top2.removeAllComponents();
		top2.addComponent(content);
		top2.setComponentAlignment(content, Alignment.MIDDLE_CENTER);
	}
	
		/////////////////////////////////////////////////////////////////
		//TAB CREATION//
	/////////////////////////////////////////////////////////////////
	
	/**
	* Adds a new tab to SideMenu Component.
	* 
	* @param label
	*            The new tab's label.
	* @return The new tab.
	*/
	
	public Tabber addTab(String text, Resource icon) {
		Tabber tab = new Tabber();
		tab.addStyleName("accord-select");
		tab.addLayoutClickListener(e ->{
			if(e.isDoubleClick()){
				removeStyleName("menu-slide-down");
				addStyleName("menu-slide-up");
			}
		});
		accord.addTab(tab, text, icon);
		return tab;
	}
	
	/**
	* Top-level menu item in a SideMenu.
	* 
	* @author Raji Zakariyya @ Vaadin
	*/
	public class Tabber extends VerticalLayout{
	
	private Layout lay = null;
	
	private Tabber(){
	
		addStyleName("menu-slide-down");
		
		if (lay == null) {
			lay = new VerticalLayout();
			lay.setSizeFull();
		}
		
		addComponent(lay);
	}
	
	
		/**
		* Adds a new button to this tab.
		* 
		* @param b
		*            The button to use.
		*/
		public void addButton(String text, final MenuClickHandler handler) {
		 	 
		Button b = new Button(text, new ClickListener() {
		
			@Override
			public void buttonClick(ClickEvent event) {
				handler.click();
			}
			});
			b.setPrimaryStyleName("sub-menu");
			b.setSizeFull();
			lay.addComponent(b);
		
		}
	
	}
}

