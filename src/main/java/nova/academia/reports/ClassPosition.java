package nova.academia.reports;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.vaadin.server.VaadinSession;

import nova.academia.admission.Student;
import nova.academia.util.Controller;

public class ClassPosition {
	
	private Controller c;
	private Score score;
	private ArrayList<Integer> other_mid = new ArrayList<>();//Arays holding holding mid term scores
	private ArrayList<Integer> other_exam = new ArrayList<>();//Arrays holding exam scores
	private ArrayList<Integer> total = new ArrayList<>();//Arrays holding assessment totals
	private ArrayList<Integer> other_total = new ArrayList<>();
	private ArrayList<Integer> my_mid = new ArrayList<>();
	private ArrayList<Integer> my_exam = new ArrayList<>();
	private ArrayList<Integer> my_total = new ArrayList<>();
	private int my_subject_total, other_subject_total;
	
	
	public int getPosition(Student std, String term, String session){
		
		int pos = 0;
		
		List<Score> scores = new ArrayList<>();
		c = new Controller();
		JSONObject o = new JSONObject();
		try {
			o.put("tenantID", VaadinSession.getCurrent().getAttribute("tenantID"));
		
		o.put("klass", std.getKlass());
		o.put("term", term);
		o.put("session", session);
		String response = c.postRequest("admin/reportsheet", o);
		
		System.out.println("Printing Results : " + response);
		
		if(response != null){
			JSONArray b = new JSONArray();
			JSONObject jp = new JSONObject(response);
			JSONArray aj = jp.getJSONArray("response");
			
			for(int i = 0; i < aj.length(); i++){
				score = new Score();
				JSONObject a = aj.getJSONObject(i);
				
				score.setSubject(a.getString("subject"));

				if(a.getString("assess").equalsIgnoreCase("mid")){
					b = a.getJSONArray("mark");
					for(int s = 0; s < b.length(); s++){
    					JSONObject n = b.getJSONObject(s);
    					if(!n.getString("name").contains(std.getSurname())){
    						other_mid.add(Integer.parseInt(n.getString("score")));
    					}
    					else{
    						my_mid.add(Integer.parseInt(n.getString("score")));
    					}
					}
				}
				
				
				if(a.getString("assess").equalsIgnoreCase("exam")){
					b = a.getJSONArray("mark");
					for(int s = 0; s < b.length(); s++){
    					JSONObject n = b.getJSONObject(s);
    					if(!n.getString("name").contains(std.getSurname())){
    						other_exam.add(Integer.parseInt(n.getString("score")));
    					}
    					else{
    						my_exam.add(Integer.parseInt(n.getString("score")));
    					}
					}
				}
			}
			
			for (int i = 0; i < other_exam.size(); i++){
			    total.add(other_mid.get(i) + other_exam.get(i));
			}
			for (int i = 0; i < my_exam.size(); i++){
			    my_total.add(my_mid.get(i) + my_exam.get(i));
			}
			for(int i = 0; i < my_total.size(); i++){
				my_subject_total += my_total.get(i);
			}
			for(int i = 0; i < b.length()-1; i++) {//b.length() gives number of students
				other_subject_total = 0;
				for(int j = i; j < total.size(); j+=b.length()-1){
					other_subject_total += total.get(j) ;
				}
				other_total.add(other_subject_total);
			}
			
			
			System.out.println("OTHER MID : "+ other_mid);
			System.out.println("OTHER EXAM : "+ other_exam);
			System.out.println("OTHER Total : "+ total);
			System.out.println("OTHERs CUMULATIVE : "+ other_total);
			
			other_total.add(my_subject_total);
			
			System.out.println("MY MID : "+ my_mid);
			System.out.println("MY EXAM : "+ my_exam);
			System.out.println("MY Total : "+ my_total);
			System.out.println("MY CUMULATIVE : "+ my_subject_total);
			System.out.println("EVERYTHING TOGETHER : " + other_total);
			
			Comparator mycomparator = Collections.reverseOrder();
			
			other_total.sort(mycomparator);
			System.out.println("AFTER SORTING : " + other_total);
			
			pos = other_total.indexOf(my_subject_total) + 1;
			System.out.println("CLASS POSITION : " + pos);
		}
		
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
		return pos;
	}
	
		
	public int getNumberInClass(Student std){
		
		int total = 0;
		
		c = new Controller();
		JSONObject o = new JSONObject();
		try {
			o.put("tenantID", VaadinSession.getCurrent().getAttribute("tenantID"));
			
			o.put("klass", std.getKlass());
			String response = c.postRequest("admin/students", o);
			
			if(response != null){
				
				JSONObject jp = new JSONObject(response);
				JSONArray aj = jp.getJSONArray("response");
				total = aj.length();
			}
		}
		catch(Exception e){
			e.printStackTrace();
		}
		return total;
	}

}
