package nova.academia.reports;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.vaadin.data.Item;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.data.util.GeneratedPropertyContainer;
import com.vaadin.data.util.PropertyValueGenerator;
import com.vaadin.data.util.filter.SimpleStringFilter;
import com.vaadin.server.StreamResource;
import com.vaadin.server.VaadinSession;
import com.vaadin.ui.Button;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.Component;
import com.vaadin.ui.Grid;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.renderers.ButtonRenderer;
import com.vaadin.ui.renderers.ClickableRenderer.RendererClickEvent;
import com.vaadin.ui.themes.ValoTheme;


import com.vaadin.ui.Grid.HeaderCell;
import com.vaadin.ui.Grid.HeaderRow;
import com.vaadin.ui.Grid.SingleSelectionModel;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;

import nova.academia.admission.Student;
import nova.academia.util.Controller;
import nova.academia.util.Report;
import org.vaadin.simplefiledownloader.SimpleFileDownloader;


public class ReportSheet extends VerticalLayout{

	
	private Grid grid;
	private BeanItemContainer<Student> container;
	private Controller c;
	private Student user;
	private ComboBox klass;
	private ComboBox term;
	private ComboBox session;
	private Button show;
	
        private SimpleFileDownloader download = new SimpleFileDownloader();
	
	public ReportSheet() {
		
		setPrimaryStyleName("material");
		addStyleName("setup-view");
		setWidth("80%");
		setMargin(true);
		setSpacing(true);
		
		Label title = new Label("Report Sheet");
		title.setPrimaryStyleName("view-title");
		addComponent(title);
		
		addComponent(buildCriteria());
		addComponent(buildTable());
		
				
		try {
			onLoad();
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}
	
	@SuppressWarnings("deprecation")
	private Component buildCriteria(){
		
		HorizontalLayout lay = new HorizontalLayout();
		lay.setSizeFull();
		
		klass = new ComboBox("Select Class");
		lay.addComponent(klass);
		
		term = new ComboBox("Select Term");
		lay.addComponent(term);
		
		session = new ComboBox("Select Session");
		lay.addComponent(session);
		
		show = new Button("Show Report");
		show.addStyleName(ValoTheme.BUTTON_DANGER);
		lay.addComponent(show);
	
		show.addClickListener(e ->{
			try {
				pullStudents();
			} catch (JSONException e1) {
				e1.printStackTrace();
			}
		});
		
		return lay;
	}
	
	private Component buildTable(){
		container = new BeanItemContainer<Student>(Student.class);
		
		// Generate button caption column
		GeneratedPropertyContainer gpc = new GeneratedPropertyContainer(container);
		gpc.addGeneratedProperty("print", new PropertyValueGenerator<String>() {
			@Override
			public String getValue(Item item, Object itemId, Object propertyId) {
				return "Print"; // The caption
			}
			@Override
			public Class<String> getType() {
				return String.class;
			}
		});
				
		grid = new Grid(gpc);
		
		// Render a button that deletes the data row (item)
		grid.getColumn("print").setRenderer(
			new ButtonRenderer(e -> // Java 8
				invokeReport(e)
			)
		);
		
		hideColumn(grid);
		
		grid.setSizeFull();
		grid.setResponsive(true);
		
		Grid.Column regColumn = grid.getColumn("regno");
		regColumn.setHeaderCaption("Registeration No.");
		
		Grid.Column firstColumn = grid.getColumn("surname");
		firstColumn.setHeaderCaption("First Name");
		
		Grid.Column middleColumn = grid.getColumn("middle");
		middleColumn.setHeaderCaption("Middle Name");
				
		Grid.Column otherColumn = grid.getColumn("other");
		otherColumn.setHeaderCaption("Last Name");
		
		grid.setColumnOrder("regno", "surname", "middle", "other", "print");
		

		filterProvider(grid);
		
		return grid;
	}
	
	private Object invokeReport(RendererClickEvent e) {
		
		boolean me = ((SingleSelectionModel)grid.getSelectionModel()).select(e.getItemId());
		
		if(me){
			Student selected = (Student)((SingleSelectionModel)grid.getSelectionModel()).getSelectedRow();
			try {
				Report card = new Report(selected, term.getValue().toString(), session.getValue().toString());//for printing report
                                ByteArrayOutputStream stream = card.build(selected, term.getValue().toString(), session.getValue().toString());
                                
                                final StreamResource resource = new StreamResource(() -> {
                                try {
                                        return new ByteArrayInputStream(stream.toByteArray());
                                    } catch (Exception e1) {
                                        e1.printStackTrace();
                                    }
                                    return null;
                                }, "KedcoPOS General Report.pdf");

                                download.setFileDownloadResource(resource);
                                download.download();
//				new ClassPosition().getPosition(selected, term.getValue().toString(), session.getValue().toString());
				
			} catch (Exception e1) {
				
				e1.printStackTrace();
			}
		}
		
		return null;
	}

	private void onLoad() throws JSONException{
		c = new Controller();
		JSONObject o = new JSONObject();
		o.put("tenantID", VaadinSession.getCurrent().getAttribute("tenantID"));
		String response = c.postRequest("admin/assesses", o);
		String response2 = c.postRequest("admin/classes", o);
		
		if(response != null && response2 != null){
			
			JSONObject jp = new JSONObject(response);
			JSONArray aj = jp.getJSONArray("response");
			
			JSONObject ab = new JSONObject(response2);
			JSONArray b = ab.getJSONArray("response");
			
			for(int i = 0; i < b.length(); i++){
				JSONObject k = b.getJSONObject(i);
				klass.addItem(k.getString("klassName"));
				
			}
			for(int j = 0; j < aj.length(); j++ ){
				JSONObject d = aj.getJSONObject(j);
				term.addItem(d.getString("term"));
				session.addItem(d.getString("session"));
			}
				
		}
	}

	
	private void pullStudents() throws JSONException{
		c = new Controller();
		container.removeAllItems();
		
		JSONObject o = new JSONObject();
		o.put("tenantID", VaadinSession.getCurrent().getAttribute("tenantID"));
		o.put("klass", klass.getValue());
		String response = c.postRequest("admin/allstudent", o);
		
		if(response != null){
			
			JSONObject jp = new JSONObject(response);
			JSONArray aj = jp.getJSONArray("response");
			
			for(int i = 0; i < aj.length(); i++){
				user = new Student();
				JSONObject j = aj.getJSONObject(i);
				user.setId(j.getLong("id"));
				user.setRegno(j.getString("regno"));
				user.setSurname(j.getString("surname"));
				user.setMiddle(j.getString("middle"));
				user.setOther(j.getString("other"));
				user.setKlass(j.getString("klass"));
				user.setTenantID(j.getString("tenantID"));
				user.setAddress(j.getString("address"));
				user.setComment(j.getString("comment"));
				user.setCountry(j.getString("country"));
				user.setDates(j.getString("date"));
				user.setEmail(j.getString("email"));
				user.setFullname(j.getString("fullname"));
				user.setGender(j.getString("gender"));
				user.setHouse(j.getString("house"));
				user.setLga(j.getString("lga"));
				user.setPhone(j.getString("phone"));
				user.setState(j.getString("state"));
				user.setStatus(j.getString("status"));
				user.setStudentID(j.getString("studentID"));
				
				container.addBean(user);
				
			}
						
//			grid.setContainerDataSource(container);
		}
		
	}
	
	private void filterProvider(Grid grid){

				HeaderRow filterRow = grid.appendHeaderRow();
				for (Object pid: grid.getContainerDataSource().getContainerPropertyIds()) {
					if(pid != "print"){
						HeaderCell cell = filterRow.getCell(pid);
						TextField filterField = new TextField();
						filterField.setHeight("25px");
						filterField.setColumns(7 -(1));
						filterField.addTextChangeListener(change -> {
						container.removeContainerFilters(pid);
							if (!change.getText().isEmpty())
								container.addContainerFilter(new SimpleStringFilter(pid, change.getText(), true, false));
						});
						
						cell.setComponent(filterField);
					}
					
				}
	}
	
	private void hideColumn(Grid grid){
		
		grid.getColumn("id").setHidden(true);
		grid.getColumn("tenantID").setHidden(true);
		grid.getColumn("klass").setHidden(true);
		grid.getColumn("address").setHidden(true);
		grid.getColumn("comment").setHidden(true);
		grid.getColumn("country").setHidden(true);
		grid.getColumn("dates").setHidden(true);
		grid.getColumn("email").setHidden(true);
		grid.getColumn("fullname").setHidden(true);
		grid.getColumn("gender").setHidden(true);
		grid.getColumn("house").setHidden(true);
		grid.getColumn("lga").setHidden(true);
		grid.getColumn("phone").setHidden(true);
		grid.getColumn("state").setHidden(true);
		grid.getColumn("studentID").setHidden(true);
		grid.getColumn("status").setHidden(true);
		grid.getColumn("logo").setHidden(true);
		
	}

	
}

