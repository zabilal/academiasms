package nova.academia.reports;

public class Score {

	private String subject, asess;
	private int cw, hw, mid, exam, score;
	
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public int getCw() {
		return cw;
	}
	public void setCw(int cw) {
		this.cw = cw;
	}
	public int getHw() {
		return hw;
	}
	public void setHw(int hw) {
		this.hw = hw;
	}
	public int getMid() {
		return mid;
	}
	public void setMid(int mid) {
		this.mid = mid;
	}
	public int getExam() {
		return exam;
	}
	public void setExam(int exam) {
		this.exam = exam;
	}
	public String getAsess() {
		return asess;
	}
	public void setAsess(String asess) {
		this.asess = asess;
	}
	public int getScore() {
		return score;
	}
	public void setScore(int score) {
		this.score = score;
	}
	@Override
	public String toString() {
		return "Score [subject=" + subject + ", asess=" + asess + ", cw=" + cw + ", hw=" + hw + ", mid=" + mid
				+ ", exam=" + exam + ", score=" + score + "]";
	}
	
	
}
