 package nova.school;

import java.util.Locale;

import javax.servlet.annotation.WebServlet;

import com.vaadin.annotations.Theme;
import com.vaadin.annotations.VaadinServletConfiguration;
import com.vaadin.annotations.Viewport;
import com.vaadin.server.Responsive;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinServlet;
import com.vaadin.server.VaadinSession;
import com.vaadin.ui.Button;
import com.vaadin.ui.Label;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.ValoTheme;

import nova.academia.mainview.LoginView;
import nova.academia.mainview.MainView;
import nova.academia.nav.SideMenu;
import nova.academia.nav.SideMenuUI;
import nova.academia.settings.entity.User;
import nova.academia.util.Storage;


/**
 * This UI is the application entry point. A UI may either represent a browser window 
 * (or tab) or some part of a html page where a Vaadin application is embedded.
 * <p>
 * The UI is initialized using {@link #init(VaadinRequest)}. This method is intended to be 
 * overridden to add component to the user interface and initialize non-component functionality.
 */
@Theme("mytheme")
@SideMenuUI
@Viewport("width=device-width, initial-scale=1")
public class SchoolUI extends UI {
        
	
		private SideMenu sideMenu = new SideMenu();
		
        @Override
        protected void init(VaadinRequest vaadinRequest) {
        	setLocale(Locale.US);

            Responsive.makeResponsive(this);
            addStyleName(ValoTheme.UI_WITH_MENU);
            
        	updateContent();
        	
        }

        private void updateContent() {
 
        	String id1 = (String)VaadinSession.getCurrent().getAttribute("userID");
        	String id2 = (String)VaadinSession.getCurrent().getAttribute("tenantID");
        	
            if(id1 != null && id2 != null){
            	setContent(new MainView());
	            removeStyleName("loginview");
//	            getNavigator().navigateTo(getNavigator().getState());
            }
            else {
                setContent(new LoginView());
                addStyleName("loginview");
            }
        }

    @WebServlet(urlPatterns = "/*", name = "SchoolUIServlet", asyncSupported = true)
    @VaadinServletConfiguration(ui = SchoolUI.class, productionMode = false)
    public static class SchoolUIServlet extends VaadinServlet {
    }
}
