package nova.school;

import com.vaadin.server.FontAwesome;
import com.vaadin.server.Resource;
import com.vaadin.server.ThemeResource;
import com.vaadin.ui.Accordion;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Layout;
import com.vaadin.ui.Notification;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.ValoTheme;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.TabSheet.SelectedTabChangeEvent;
import com.vaadin.ui.TabSheet.SelectedTabChangeListener;

public class NovaSide extends CssLayout{

	public NovaSide(){
		
		addComponent(buildLoginInformation());
		setPrimaryStyleName(ValoTheme.MENU_PART);
	}
	
	 private CssLayout buildLoginInformation() {
	        CssLayout loginInformation = new CssLayout();
	        loginInformation.setStyleName("login-information");
	        
	        loginInformation.addComponent(profile());
	        loginInformation.addComponent(menu());
	        
	        return loginInformation;
	    }
	 
	 private CssLayout profile(){
	    	
	    	CssLayout pro = new CssLayout();
	    	VerticalLayout v = new VerticalLayout();
	    	v.setWidth("250px");
	    	
	    	pro.setPrimaryStyleName("profile");
	    	    	
	    	Resource icon = new ThemeResource("favicon.ico");
	    	Label name = new Label("Raji Zakariyya");
	    	name.setSizeUndefined();
	    	Label ic = new Label();
	    	ic.setSizeUndefined();
	    	ic.setIcon(icon);
	    	
	    	v.addComponents(ic, name);
	    	v.setComponentAlignment(ic, Alignment.MIDDLE_CENTER);
	    	v.setComponentAlignment(name, Alignment.MIDDLE_CENTER);
	    	pro.addComponents(v);
	    	    	
	    	return pro;
	    }
	 
	 private Accordion menu(){
     	
     	CssLayout lay = new CssLayout();
     	
     	Accordion accordion = new Accordion();
     	accordion.setPrimaryStyleName("accordion");
     	
     	Layout tab1 = new VerticalLayout();
     	tab1.setPrimaryStyleName("menu-slide-down");
     	for(int i = 0; i < 2; i++){
     		Button b = new Button("Menu "+ i+1, (ClickListener) event -> 
     			Notification.show("Button " + event.getComponent().getCaption())
     		);
     		b.setPrimaryStyleName("sub-menu");
     		tab1.addComponent(b);
     	}
     	
     	Layout tab2 = new VerticalLayout();
     	tab2.setPrimaryStyleName("menu-slide-down");
     	for(int i = 0; i < 5; i++){
     		Button b = new Button("Menu "+ i+1);
     		b.setPrimaryStyleName("sub-menu");
     		tab2.addComponent(b);
     	}
     	
     	Layout tab3 = new VerticalLayout();
     	tab3.setPrimaryStyleName("menu-slide-down");
     	for(int i = 0; i < 5; i++){
     		Button b = new Button("Menu "+ i+1);
     		b.setPrimaryStyleName("sub-menu");
     		tab3.addComponent(b);
     	}
     	
     	Layout tab4 = new VerticalLayout();
     	tab4.setPrimaryStyleName("menu-slide-down");
     	for(int i = 0; i < 5; i++){
     		Button b = new Button("Menu "+ i+1);
     		b.setPrimaryStyleName("sub-menu");
     		tab4.addComponent(b);
     	}
     	
     	accordion.addTab(tab1, "Mama Tab", FontAwesome.USERS);
     	accordion.addTab(tab2, "Papa Tab", FontAwesome.ANDROID);
     	accordion.addTab(tab3, "Pikin Tab", FontAwesome.ALIGN_JUSTIFY);
     	accordion.addTab(tab4, "Family Tab", FontAwesome.AREA_CHART);
     	
     	accordion.addSelectedTabChangeListener(new SelectedTabChangeListener() {
     	
     		public void selectedTabChange(SelectedTabChangeEvent event) {
		        	Notification.show("You are watching "
		        	+ event.getTabSheet().getSelectedTab());
		        	}
     	});
	        
	        lay.addComponent(accordion);
	        return accordion;
 }
}
