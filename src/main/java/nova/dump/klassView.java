package nova.academia.admission;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.server.Responsive;
import com.vaadin.server.VaadinSession;
import com.vaadin.ui.Button;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.Component;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.Grid;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Notification;
import com.vaadin.ui.PasswordField;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Grid.SingleSelectionModel;
import com.vaadin.ui.Notification.Type;
import com.vaadin.ui.themes.ValoTheme;

import nova.academia.util.Controller;
import nova.academia.util.State;
import nova.academia.util.Storage;

public class klassView extends HorizontalLayout{

	private static boolean isEdit;
	private Controller c;
	private klass user;
	private BeanItemContainer<klass> container;
	

	private TextField klassname;
	private TextField previous;
	private TextField next;
	private TextField teacher;
	private Grid grid;
	
	public static boolean isEdit() {
		return isEdit;
	}

	public static void setEdit(boolean isEdit) {
		klassView.isEdit = isEdit;
	}

	
	public klassView(){
		
		Responsive.makeResponsive(this);
		
		setWidth("100%");
		setSizeFull();
		setMargin(true);
		setSpacing(true);
		//setComponentAlignment(form, Alignment.MIDDLE_CENTER);
		addStyleName("setup-view");
		addStyleName("material");
			
		addComponent(buildUser());
		addComponent(buildTable());
		
		try {
			onLoad();
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}
	
	private FormLayout buildUser(){
		
		FormLayout form = new FormLayout();
		form.setSizeFull();
		form.setCaption("Add Class");
		
		klassname = new TextField("Class Name");
		klassname.setWidth("80%");
		form.addComponent(klassname);
		
		previous = new TextField("Previous Class");
		previous.setWidth("80%");
		form.addComponent(previous);
		
		next = new TextField("Next Class");
		next.setWidth("80%");
		form.addComponent(next);
		
		teacher = new TextField("Class Teacher");
		teacher.setWidth("80%");
		form.addComponent(teacher);
				
		HorizontalLayout hb = new HorizontalLayout();
		hb.setSpacing(true);
		hb.setMargin(true);
		hb.setWidth("80%");
		
		Button save = new Button("Add Class");
		save.addClickListener(event -> {
			try {
				fireSaveEvent();
			} catch (JSONException e) {
				e.printStackTrace();
			}
		});
		save.addStyleName(ValoTheme.BUTTON_FRIENDLY);
		
				
		hb.addComponent(save);
		
		form.addComponent(hb);
		
				
		return form;
		
		
	}
	
	private Component buildTable(){
	
		container = new BeanItemContainer<klass>(klass.class);
		
		// Create a grid
		grid = new Grid("Class List");
	
		grid.addColumn("klassName");
		grid.addColumn("previous");
		grid.addColumn("next");
		grid.addColumn("teacher");
		
		Grid.Column a = grid.getColumn("klassName");
		a.setHeaderCaption("Class Name");
		
		Grid.Column b = grid.getColumn("previous");
		b.setHeaderCaption("Previous Class");
		
		Grid.Column c = grid.getColumn("next");
		c.setHeaderCaption("Next Class");
		
		Grid.Column d = grid.getColumn("teacher");
		d.setHeaderCaption("Class Teacher");
		
		grid.setSizeFull();
		grid.setResponsive(true);
		grid.setEditorEnabled(true);
		
		grid.addSelectionListener(selectionEvent -> { 
			
			klass selected = (klass)((SingleSelectionModel)grid.getSelectionModel()).getSelectedRow();
			if (selected != null){
				klassname.setValue(selected.getKlassName());
				previous.setValue(selected.getPrevious());
				next.setValue(selected.getNext());
				teacher.setValue(selected.getTeacher());
				Storage.setUserid(selected.getKlassID());
				setEdit(true);
			}
			else{
				Notification.show("Nothing selected");
			}
		});
		
		return grid;
	}
	
	private void fireSaveEvent() throws JSONException {
		c = new Controller();
		
		JSONObject json = new JSONObject();
		json.put("tenantID", VaadinSession.getCurrent().getAttribute("tenantID"));
		json.put("klassName", klassname.getValue());
		json.put("previous", previous.getValue());
		json.put("next", next.getValue());
		json.put("teacher", teacher.getValue());

		
		if(!isEdit){
			System.out.println("Saving Object");
			String h = c.postRequest("admin/class", json);
			JSONObject oj = new JSONObject(h);
			JSONObject j = oj.getJSONObject("response");
			if(j != null){
				Notification.show("Class Saved", Type.HUMANIZED_MESSAGE);
				clear();
				onLoad();
			}
		}else{
			json.put("klassID", Storage.getUserid());
			String h = c.postRequest("admin/classus", json);
			JSONObject j = new JSONObject(h);
			if(j.getInt("response") == 1){
				Notification.show("Class Updated", Type.HUMANIZED_MESSAGE);
				onLoad();
				clear();
//				setEdit(false);
			}else{
				Notification.show("Class Failed to Update", Type.HUMANIZED_MESSAGE);
			}
		}
	}
	
	private void onLoad() throws JSONException{
		c = new Controller();
		container.removeAllItems();
		
		JSONObject o = new JSONObject();
		o.put("tenantID", VaadinSession.getCurrent().getAttribute("tenantID"));
		String response = c.postRequest("admin/classes", o);
		
		if(response != null){
			
			JSONObject jp = new JSONObject(response);
			JSONArray aj = jp.getJSONArray("response");
			
			for(int i = 0; i < aj.length(); i++){
				user = new klass();
				JSONObject j = aj.getJSONObject(i);
				user.setKlassID(j.getString("klassID"));
				user.setKlassName(j.getString("klassName"));
				user.setPrevious(j.getString("previous"));
				user.setNext(j.getString("next"));
				user.setTeacher(j.getString("teacher"));
				
				container.addBean(user);
			}
						
			grid.setContainerDataSource(container);
		}
		
	}
	
	private void clear(){
		setEdit(false);
		klassname.setValue("");
		previous.setValue("");
		next.setValue("");
		teacher.setValue("");
	}
	
}
