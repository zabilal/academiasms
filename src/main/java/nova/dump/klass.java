package nova.academia.admission;

public class klass {

	private String klassID;
	private String klassName;
	private String category;
	private String previous;
	private String next;
	private String teacher;
	public String getKlassID() {
		return klassID;
	}
	public void setKlassID(String klassID) {
		this.klassID = klassID;
	}
	public String getKlassName() {
		return klassName;
	}
	public void setKlassName(String klassName) {
		this.klassName = klassName;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public String getPrevious() {
		return previous;
	}
	public void setPrevious(String previous) {
		this.previous = previous;
	}
	public String getNext() {
		return next;
	}
	public void setNext(String next) {
		this.next = next;
	}
	public String getTeacher() {
		return teacher;
	}
	public void setTeacher(String teacher) {
		this.teacher = teacher;
	}
	@Override
	public String toString() {
		return "klass [klassID=" + klassID + ", klassName=" + klassName + ", category=" + category + ", previous="
				+ previous + ", next=" + next + ", teacher=" + teacher + "]";
	}
	
	
}
