package nova.academia.admission;

public class Grade {

	private long id;
	private String gradeID, title, code, startMark, endMark;
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getGradeID() {
		return gradeID;
	}
	public void setGradeID(String gradeID) {
		this.gradeID = gradeID;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getStartMark() {
		return startMark;
	}
	public void setStartMark(String startMark) {
		this.startMark = startMark;
	}
	public String getEndMark() {
		return endMark;
	}
	public void setEndMark(String endMark) {
		this.endMark = endMark;
	}
	@Override
	public String toString() {
		return "Grade [id=" + id + ", gradeID=" + gradeID + ", title=" + title + ", code=" + code + ", startMark="
				+ startMark + ", endMark=" + endMark + "]";
	}
	
	
}
