package nova.academia.admission;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.server.Responsive;
import com.vaadin.server.VaadinSession;
import com.vaadin.ui.Button;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.Component;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.Grid;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Notification;
import com.vaadin.ui.PasswordField;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Grid.SingleSelectionModel;
import com.vaadin.ui.Notification.Type;
import com.vaadin.ui.themes.ValoTheme;

import nova.academia.util.Controller;
import nova.academia.util.State;
import nova.academia.util.Storage;

public class GradeView extends HorizontalLayout{

	private static boolean isEdit;
	private Controller c;
	private Grade user;
	private BeanItemContainer<Grade> container;
	

	private TextField title;
	private TextField code;
	private TextField startMark;
	private TextField endMark;
	private Grid grid;
	
	public static boolean isEdit() {
		return isEdit;
	}

	public static void setEdit(boolean isEdit) {
		GradeView.isEdit = isEdit;
	}

	
	public GradeView(){
		
		Responsive.makeResponsive(this);
		
		setWidth("100%");
		setSizeFull();
		setMargin(true);
		setSpacing(true);
		//setComponentAlignment(form, Alignment.MIDDLE_CENTER);
		addStyleName("setup-view");
		addStyleName("material");
			
		addComponent(buildUser());
		addComponent(buildTable());
		
		try {
			onLoad();
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}
	
	private FormLayout buildUser(){
		
		FormLayout form = new FormLayout();
		form.setSizeFull();
		form.setCaption("New Grade");
		
		code = new TextField("Grade Code");
		code.setWidth("80%");
		form.addComponent(code);
		
		title = new TextField("Grade Title");
		title.setWidth("80%");
		form.addComponent(title);
		
		startMark = new TextField("Lowest Mark");
		startMark.setWidth("80%");
		form.addComponent(startMark);
		
		endMark = new TextField("Highest Mark");
		endMark.setWidth("80%");
		form.addComponent(endMark);
				
		HorizontalLayout hb = new HorizontalLayout();
		hb.setSpacing(true);
		hb.setMargin(true);
		hb.setWidth("80%");
		
		Button save = new Button("Add Grade");
		save.addClickListener(event -> {
			try {
				fireSaveEvent();
			} catch (JSONException e) {
				e.printStackTrace();
			}
		});
		save.addStyleName(ValoTheme.BUTTON_FRIENDLY);
		
				
		hb.addComponent(save);
		
		form.addComponent(hb);
		
				
		return form;
		
		
	}
	
	private Component buildTable(){
	
		container = new BeanItemContainer<Grade>(Grade.class);
		
		// Create a grid
		grid = new Grid("Grade List");
	
		grid.addColumn("title");
		grid.addColumn("code");
		grid.addColumn("startMark");
		grid.addColumn("endMark");
		
		Grid.Column a = grid.getColumn("title");
		a.setHeaderCaption("Grade Title");
		
		Grid.Column b = grid.getColumn("code");
		b.setHeaderCaption("Grade Code");
		
		Grid.Column c = grid.getColumn("startMark");
		c.setHeaderCaption("Lowest Mark");
		
		Grid.Column d = grid.getColumn("endMark");
		d.setHeaderCaption("Highest Mark");
		
		grid.setSizeFull();
		grid.setResponsive(true);
		grid.setEditorEnabled(true);
		
		grid.addSelectionListener(selectionEvent -> { 
			
			Grade selected = (Grade)((SingleSelectionModel)grid.getSelectionModel()).getSelectedRow();
			if (selected != null){
				title.setValue(selected.getTitle());
				code.setValue(selected.getCode());
				startMark.setValue(selected.getStartMark());
				endMark.setValue(selected.getEndMark());
				Storage.setUserid(selected.getGradeID());
				setEdit(true);
			}
			else{
				Notification.show("Nothing selected");
			}
		});
		
		return grid;
	}
	
	private void fireSaveEvent() throws JSONException {
		c = new Controller();
		
		JSONObject json = new JSONObject();
		json.put("tenantID", VaadinSession.getCurrent().getAttribute("tenantID"));
		json.put("title", title.getValue());
		json.put("code", code.getValue());
		json.put("startMark", startMark.getValue());
		json.put("endMark", endMark.getValue());

		
		if(!isEdit){
			System.out.println("Saving Object");
			String h = c.postRequest("admin/grade", json);
			JSONObject oj = new JSONObject(h);
			JSONObject j = oj.getJSONObject("response");
			if(j != null){
				Notification.show("Grade Saved", Type.HUMANIZED_MESSAGE);
				clear();
				onLoad();
			}
		}else{
			json.put("gradeID", Storage.getUserid());
			String h = c.postRequest("admin/gradu", json);
			JSONObject j = new JSONObject(h);
			if(j.getInt("response") == 1){
				Notification.show("Grade Updated", Type.HUMANIZED_MESSAGE);
				onLoad();
				clear();
//				setEdit(false);
			}else{
				Notification.show("Grade Failed to Update", Type.HUMANIZED_MESSAGE);
			}
		}
	}
	
	private void onLoad() throws JSONException{
		c = new Controller();
		container.removeAllItems();
		
		JSONObject o = new JSONObject();
		o.put("tenantID", VaadinSession.getCurrent().getAttribute("tenantID"));
		String response = c.postRequest("admin/grades", o);
		
		if(response != null){
			
			JSONObject jp = new JSONObject(response);
			JSONArray aj = jp.getJSONArray("response");
			
			for(int i = 0; i < aj.length(); i++){
				user = new Grade();
				JSONObject j = aj.getJSONObject(i);
				user.setGradeID(j.getString("gradeID"));
				user.setCode(j.getString("code"));
				user.setEndMark(j.getString("endMark"));
				user.setStartMark(j.getString("startMark"));
				user.setTitle(j.getString("title"));
				
				container.addBean(user);
			}
						
			grid.setContainerDataSource(container);
		}
		
	}
	
	private void clear(){
		setEdit(false);
		title.setValue("");
		code.setValue("");
		startMark.setValue("");
		endMark.setValue("");
	}
	
}
